"""Implements a weighted queue for running tasks

The weighted tasks give us two levels of control on the parallelism involved. The
first control is the parallelism of the Executor. This, on its own, is insufficient
for reindexing as the reindex tasks use limited local resources and can use significant
cluster resources. The weighting allows the paralleism to be restricted when running
heavy tasks, while also allowing many tasks in parallel when they are light weight.

Tasks are run heaviest to lightest, with lighter tasks filling in the spare capacity
where available.
"""

from concurrent.futures import ALL_COMPLETED, FIRST_COMPLETED, Executor, Future, wait
from typing import (
    Any,
    Callable,
    Dict,
    Generic,
    Iterator,
    List,
    Optional,
    Tuple,
    TypeVar,
)


class Task:
    def __call__(self) -> Any:
        raise NotImplementedError()

    @property
    def weight(self) -> int:
        raise NotImplementedError()


T = TypeVar("T", contravariant=True, bound=Task)


class FutureCollection(Generic[T]):
    executor: Executor
    futures: Dict[Future, T]

    def __init__(self, executor: Executor):
        self.executor = executor
        self.futures = dict()

    def __len__(self) -> int:
        return len(self.futures)

    @property
    def used_capacity(self) -> int:
        return sum(task.weight for task in self.futures.values())

    def submit(self, task: T) -> None:
        fn: Callable[[], Any] = task
        future = self.executor.submit(fn)
        self.futures[future] = task

    def wait(
        self, return_when: str, timeout: Optional[int] = None
    ) -> Iterator[Tuple[T, Any]]:
        result = wait(self.futures.keys(), return_when=return_when, timeout=timeout)
        for future in result.done:
            task = self.futures.pop(future)
            if future.cancelled():
                # Cancelled means it was never started, likely because shutdown
                # was requested. Hope that false is meaningful to consumer...
                yield task, False
            else:
                # TODO: If task threw, then future.result() is going to throw
                yield task, future.result()

    def shutdown(self) -> None:
        self.executor.shutdown(wait=False)
        # Cancel means tasks that were submitted but not started. Any started
        # task cannot be canceled and will run to completion.
        for future in self.futures.keys():
            future.cancel()


class WorkQueue(Generic[T]):
    futures: FutureCollection[T]
    max_capacity: int
    max_parallelism: int
    queue: List[T]

    def __init__(self, executor: Executor, max_capacity: int, max_parallelism: int):
        self.futures = FutureCollection(executor)
        self.max_capacity = max_capacity
        self.max_parallelism = max_parallelism
        self.queue = []

    def add(self, task: T) -> None:
        self.queue.append(task)

    @property
    def available_capacity(self):
        return self.max_capacity - self.futures.used_capacity

    def _execute_pending(self) -> None:
        # Sort by weight, largest first. This ensures large tasks run at the
        # beginning, and when one large task finishes the next largest task has
        # space to run. heapq might be more efficient, but this is simple.
        self.queue.sort(key=lambda task: task.weight, reverse=True)

        if self.queue[0].weight > self.max_capacity:
            # We have one (or more) oversized tasks. Run these one at a time
            # until the queue contains only items that fit.
            if not self.futures:
                task = self.queue.pop(0)
                self.futures.submit(task)
        else:
            # queue up smaller and smaller tasks until we use up the capacity
            for task in self.queue:
                if task.weight > self.available_capacity:
                    continue
                if len(self.futures) >= self.max_parallelism:
                    break
                self.queue.remove(task)
                self.futures.submit(task)

    def run(self) -> Iterator[Tuple[T, Any]]:
        while True:
            if self.queue:
                self._execute_pending()

            if not self.futures:
                if self.queue:
                    raise ValueError(
                        "Nothing running, but tasks still queued. Should be impossible?"
                    )
                return

            # Wait for anything to finish and free some space. Keep a short
            # timeout to pick up shutdown requests.
            yield from self.futures.wait(FIRST_COMPLETED, timeout=5)

    def shutdown(self, wait: bool = True) -> List[Tuple[T, Any]]:
        self.futures.shutdown()
        if wait:
            return list(self.futures.wait(ALL_COMPLETED))
        else:
            return []
