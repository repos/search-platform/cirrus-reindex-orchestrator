from __future__ import annotations

import logging
import os
import subprocess
from collections.abc import Callable
from dataclasses import dataclass
from functools import cached_property
from logging import Logger
from typing import Any, Final, List, Mapping, Optional, TextIO, cast

import yaml
from kubernetes import client, config
from kubernetes.client import (
    ApiClient,
    CoreV1Api,
    CustomObjectsApi,
    V1ConfigMap,
    V1ConfigMapList,
    V1ContainerState,
    V1ContainerStateTerminated,
    V1ContainerStatus,
    V1Pod,
    V1PodList,
    V1PodStatus,
)
from kubernetes.config import list_kube_config_contexts
from kubernetes.stream import stream
from kubernetes.stream.ws_client import ERROR_CHANNEL
from kubernetes.watch import Watch


class KubectlException(Exception):
    pass


def _is_subset(expect: Mapping, other: Mapping) -> bool:
    return all(key in other and expect[key] == other[key] for key in expect)


KUBECTL_ENV_K8S_CLUSTER: Final[str] = "K8S_CLUSTER"
KUBECTL_ENV_KUBECONFIG: Final[str] = "KUBECONFIG"


logger: Logger = logging.getLogger(__name__)


@dataclass
class KubeCtl:
    k8s_client: ApiClient
    core_v1: CoreV1Api
    custom_object_api: CustomObjectsApi
    namespace: str
    _web_socket_stream_callable: Callable = stream

    @classmethod
    def from_conf(cls, conf: str, ns: Optional[str]) -> KubeCtl:
        if ns is None:
            contexts, current = list_kube_config_contexts(config_file=conf)
            namespace = current["context"]["namespace"]
        else:
            namespace = ns

        k8s_client = config.new_client_from_config(config_file=conf)
        return cls(
            k8s_client=k8s_client,
            core_v1=client.CoreV1Api(k8s_client),
            custom_object_api=client.CustomObjectsApi(k8s_client),
            namespace=namespace,
        )

    @classmethod
    def from_env(cls, env: Mapping[str, str], ns: Optional[str]) -> KubeCtl:
        return cls.from_conf(env[KUBECTL_ENV_KUBECONFIG], ns)

    @staticmethod
    def _selectors_arg(selectors: Optional[Mapping[str, str]]) -> Optional[str]:
        if selectors is None:
            return None
        return ",".join([f"{k}={v}" for k, v in selectors.items()])

    def _list_flink_deployments(
        self, labels: Optional[Mapping[str, str]] = None
    ) -> List[Mapping]:
        logger.debug(
            "list_flink_deployment %s namespace:%s labels:%s",
            self.k8s_client.configuration.host,
            self.namespace,
            labels,
        )
        resp = self.custom_object_api.list_namespaced_custom_object(
            group="flink.apache.org",
            version="v1beta1",
            namespace=self.namespace,
            plural="flinkdeployments",
            label_selector=self._selectors_arg(labels),
        )["items"]
        return resp  # type:ignore[no-any-return]

    def get_flink_deployment_status(
        self, labels: Optional[Mapping[str, str]] = None
    ) -> str:
        items = self._list_flink_deployments(labels)
        if not items:
            return "NOT_DEPLOYED"
        if len(items) > 1:
            raise ValueError(f"More than one flinkdeployment for {labels}")
        # afaik UNKNOWN can only happen very early in the deploy while it's initializing.
        return cast(str, items[0]["status"]["jobStatus"].get("state", "UNKNOWN"))

    def find_pod_name(self, labels: Optional[Mapping[str, str]] = None) -> str:
        pods_list = self.list_pods(labels)
        pods = pods_list.items
        if len(pods) == 0:
            raise KubectlException("release not found in list of pods")
        if len(pods) > 1:
            raise KubectlException(f"multiple pods found matching release: {pods}")
        return cast(str, pods[0].metadata.name)

    def list_pods(self, labels: Optional[Mapping[str, str]] = None) -> V1PodList:
        logger.debug(
            "list_pods %s namespace:%s labels:%s",
            self.k8s_client.configuration.host,
            self.namespace,
            labels,
        )
        pods_list: V1PodList = self.core_v1.list_namespaced_pod(
            namespace=self.namespace, label_selector=self._selectors_arg(labels)
        )
        return pods_list

    def find_configmap_by_suffix(
        self, suffix: str, labels: Optional[Mapping[str, str]] = None
    ) -> V1ConfigMap:
        logger.debug(
            "find_configmap_by_suffix %s namespace:%s suffix:%s labels:%s",
            self.k8s_client.configuration.host,
            self.namespace,
            suffix,
            labels,
        )
        all_cm: V1ConfigMapList = self.core_v1.list_namespaced_config_map(
            namespace=self.namespace, label_selector=self._selectors_arg(labels)
        )
        configmaps: List[V1ConfigMap] = [
            cm for cm in all_cm.items if cm.metadata.name.endswith(suffix)
        ]
        if not configmaps:
            raise KubectlException("requested configmap not found")
        if len(configmaps) > 1:
            raise KubectlException(
                "Multiple matches for {suffix}: "
                + ",".join(cm.metadata.name for cm in configmaps)
            )
        return configmaps[0]

    @staticmethod
    def is_pod_container_stopped(pod: V1Pod, container: str) -> bool:
        if pod.status.phase in {"Succeeded", "Failed"}:
            return True
        if pod.status.phase == "Unknown":
            return False
        # The pod status is Pending. Find our container and see if it's ready yet.
        if (
            not pod.status.container_statuses
        ):  # Sometimes it's None instead of an empty list.
            return False
        for container_status in pod.status.container_statuses:
            if container_status.name == container:
                return container_status.state.terminated is not None
        return False

    @staticmethod
    def is_pod_container_started(pod: V1Pod, container: str) -> bool:
        if KubeCtl.is_pod_container_stopped(pod, container):
            # assume it (has) started if it's stopped
            return True
        if pod.status.phase == "Running":
            return True
        if (
            not pod.status.container_statuses
        ):  # Sometimes it's None instead of an empty list.
            return False
        for container_status in pod.status.container_statuses:
            if container_status.name == container:
                return container_status.state.running is not None
        return False

    def wait_until_pod_container(
        self,
        labels: Mapping[str, str],
        container: str,
        test_func: Callable[[V1Pod, str], bool],
        timeout_seconds: Optional[int] = None,
    ) -> V1Pod:
        pod_list = self.list_pods(labels)
        label_selector = self._selectors_arg(labels)
        if pod_list.items and test_func(pod_list.items[0], container):
            pod: V1Pod = pod_list.items[0]
            logger.debug(
                "wait_until_pod %s namespace:%s labels:%s pod:%s started",
                self.k8s_client.configuration.host,
                self.namespace,
                label_selector,
                pod.metadata.name,
            )
            return pod

        resource_version = pod_list.metadata.resource_version

        logger.debug(
            "wait_until_pod %s namespace:%s labels:%s waiting for the container to start",
            self.k8s_client.configuration.host,
            self.namespace,
            label_selector,
        )
        w = Watch()
        my_stream = w.stream(
            self.core_v1.list_namespaced_pod,
            namespace=self.namespace,
            label_selector=label_selector,
            resource_version=resource_version,
            timeout_seconds=timeout_seconds,
        )
        for event in my_stream:
            pod = event["object"]
            if test_func(pod, container):
                logger.debug(
                    "wait_until_pod_started %s namespace:%s labels:%s pod:%s started",
                    self.k8s_client.configuration.host,
                    self.namespace,
                    label_selector,
                    pod.metadata.name,
                )
                break
        else:
            raise KubectlException("Timed out waiting for the container to start.")

        w.stop()
        return pod

    def get_pod(self, pod: str) -> V1Pod:
        logger.debug(
            "read_pod %s namespace:%s pod:%s",
            self.k8s_client.configuration.host,
            self.namespace,
            pod,
        )
        return self.core_v1.read_namespaced_pod(name=pod, namespace=self.namespace)

    @staticmethod
    def pod_container_exit_status(container: str, pod: V1Pod) -> int:
        status: V1PodStatus = pod.status
        if status is None:
            raise KubectlException("pod has no status")
        container_statuses: List[V1ContainerStatus] = status.container_statuses
        container_status: Optional[V1ContainerStatus] = None
        if container_statuses is None:
            raise KubectlException(
                f"Cannot find any container_status for the pod {pod}. "
                f"Did you wait for it to end?"
            )
        for cs in container_statuses:
            if cs.name == container:
                container_status = cs
                break

        if container_status is None:
            raise KubectlException(
                f"Cannot find status for container {container} in pod {pod}"
            )
        state: V1ContainerState = container_status.state
        if state is None:
            raise KubectlException(
                "Cannot find state for container {container} in pod {pod}"
            )

        terminated: V1ContainerStateTerminated = state.terminated
        if terminated is None:
            raise KubectlException(
                "Cannot find terminated state for container {container} in pod {pod}"
            )

        return cast(int, terminated.exit_code)

    def get_pod_logs(self, pod: str, container: str) -> str:
        logger.debug(
            "get_pod_log %s namespace:%s pod:%s container:%s",
            self.k8s_client.configuration.host,
            self.namespace,
            pod,
            container,
        )
        logs = self.core_v1.read_namespaced_pod_log(
            name=pod, namespace=self.namespace, container=container
        )
        return cast(str, logs)

    def tail_pod_logs(self, pod: str, container: str, out: TextIO) -> None:
        logger.debug(
            "tail_pod_logs %s namespace:%s pod:%s container:%s",
            self.k8s_client.configuration.host,
            self.namespace,
            pod,
            container,
        )
        w = Watch()
        for line in w.stream(
            self.core_v1.read_namespaced_pod_log,
            name=pod,
            namespace=self.namespace,
            container=container,
        ):
            out.write(line + "\n")

    def exec(self, pod: str, container: str, *command: str) -> str:
        logger.debug(
            "exec %s namespace:%s pod:%s container:%s command:%s",
            self.k8s_client.configuration.host,
            self.namespace,
            pod,
            container,
            command,
        )

        resp = self._web_socket_stream_callable(
            self.core_v1.connect_get_namespaced_pod_exec,
            pod,
            container=container,
            namespace=self.namespace,
            command=command,
            stderr=True,
            stdin=False,
            stdout=True,
            tty=False,
            _preload_content=False,
        )
        resp.run_forever()
        err: str = resp.read_channel(ERROR_CHANNEL)
        err_parsed = yaml.safe_load(err)
        resp.close()
        if err_parsed["status"] == "Success":
            return cast(str, resp.read_stdout())
        else:
            raise KubectlException(
                f"Failed to exec command {command} on {pod}:{container}, namespace: {self.namespace}: {err}"
            )


@dataclass
class Release:
    namespace: str
    cluster: str
    name: str
    _kubectl: Optional[KubeCtl] = None
    _kubectl_deploy: Optional[KubeCtl] = None

    @property
    def kube_env(self) -> Mapping[str, str]:
        return {
            KUBECTL_ENV_K8S_CLUSTER: self.cluster,
            KUBECTL_ENV_KUBECONFIG: f"/etc/kubernetes/{self.namespace}-{self.cluster}.config",
        }

    @property
    def kube_deploy_env(self) -> Mapping[str, str]:
        return {
            KUBECTL_ENV_K8S_CLUSTER: self.cluster,
            KUBECTL_ENV_KUBECONFIG: f"/etc/kubernetes/{self.namespace}-deploy-{self.cluster}.config",
        }

    @cached_property
    def kubectl(self) -> KubeCtl:
        return (
            self._kubectl
            if self._kubectl is not None
            else KubeCtl.from_env(self.kube_env, self.namespace)
        )

    @cached_property
    def kubectl_deploy(self) -> KubeCtl:
        return (
            self._kubectl_deploy
            if self._kubectl_deploy is not None
            else KubeCtl.from_env(self.kube_deploy_env, self.namespace)
        )

    def get_flink_deployment_status(self) -> str:
        return self.kubectl.get_flink_deployment_status(self._release_label())

    def _release_label(self) -> Mapping[str, str]:
        return {"release": self.name}

    def find_pod(self, labels: Optional[Mapping[str, str]] = None) -> str:
        pod_labels = {**self._release_label(), **(labels if labels is not None else {})}
        return self.kubectl.find_pod_name(pod_labels)

    def find_configmap(self, suffix: str) -> V1ConfigMap:
        return self.kubectl.find_configmap_by_suffix(suffix, self._release_label())

    def helmfile(
        self, *args: str, env: Mapping[str, str] = {}, **kwargs: Any
    ) -> subprocess.CompletedProcess:
        return subprocess.run(
            [
                os.environ.get("HELMFILE_PATH", "/usr/bin/helmfile"),
                "--environment",
                self.cluster,
                "--selector",
                f"name={self.name}",
                *args,
            ],
            env={
                "PATH": os.environ["PATH"],
                "HELM_CONFIG_HOME": os.environ["HELM_CONFIG_HOME"],
                "HELM_CACHE_HOME": os.environ["HELM_CACHE_HOME"],
                "HELM_DATA_HOME": os.environ["HELM_DATA_HOME"],
                "SUPPRESS_SAL": "true",
                **self.kube_env,
                **env,
            },
            # This ensures ctrl-c to the python process doesn't signal the child
            start_new_session=True,
            **kwargs,
        )
