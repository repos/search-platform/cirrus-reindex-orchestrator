import json
import logging
import subprocess
from collections.abc import Callable
from dataclasses import dataclass
from functools import cached_property
from logging import Logger
from typing import List, Optional, TextIO

from kubernetes.client import V1Pod

from cirrus_reindexer.k8s import KubeCtl
from cirrus_reindexer.utils import debug_print_completed_process

logger: Logger = logging.getLogger(__name__)


class MwScriptException(Exception):
    pass


@dataclass
class MwScriptExecutionResult:
    """
    Track the execution of a mwscript-k8s job. All this assumes that mwscript is scheduling a job with a parallelism
    of 1 and no automatic retries.
    """

    namespace: str
    job: str
    pod: str
    container: str
    release: str
    cluster: str
    exit_status: int
    stdout: Optional[str]

    def cleanup(self) -> None:
        logger.debug(
            "Destroying mwscript job release %s cluster:%s namespace:%s pod:%s container:%s",
            self.release,
            self.cluster,
            self.namespace,
            self.pod,
            self.container,
        )
        subprocess.run(
            [
                "helmfile",
                "--file",
                f"/srv/deployment-charts/helmfile.d/services/{self.namespace}/helmfile.yaml",
                "--environment",
                self.cluster,
                "--selector",
                f"name={self.release}",
                "destroy",
            ],
            env={
                "PATH": "/usr/bin",  # Our helmfiles use an unqualified path for helmBinary.
                "RELEASE_NAME": self.release,  # RELEASE_NAME is consumed by the helmfile template.
            },
            check=True,
        )


@dataclass
class MwScriptExecution:
    kubectl: KubeCtl
    kubectl_deploy: KubeCtl
    job: str
    container: str
    release: str
    cluster: str

    @cached_property
    def _job_labels(self):
        return {"job-name": self.job}

    def _wait_for_pod(self) -> V1Pod:
        return self.kubectl.wait_until_pod_container(
            labels=self._job_labels,
            container=self.container,
            test_func=KubeCtl.is_pod_container_started,
            timeout_seconds=300,
        )

    def _tail_logs(self, pod: str, out: TextIO) -> None:
        self.kubectl.tail_pod_logs(pod=pod, container=self.container, out=out)

    def _wait_for_end(self) -> V1Pod:
        return self.kubectl.wait_until_pod_container(
            labels=self._job_labels,
            container=self.container,
            test_func=KubeCtl.is_pod_container_started,
            timeout_seconds=None,
        )

    def _get_pod_logs(self, pod: str) -> str:
        return self.kubectl.get_pod_logs(pod=pod, container=self.container)

    def follow(
        self, stdout: Optional[TextIO], capture_stdout: bool
    ) -> MwScriptExecutionResult:
        # we don't monitor the job itself but rather the corresponding pon and its mediawiki container.
        # we assume that mwscript jobs are non-concurrent and will never retry on their own
        pod = self._wait_for_pod()
        if stdout:
            self._tail_logs(pod.metadata.name, stdout)
        pod = self._wait_for_end()
        logs = self._get_pod_logs(pod.metadata.name) if capture_stdout else None

        return MwScriptExecutionResult(
            namespace=self.kubectl.namespace,
            job=self.job,
            pod=pod.metadata.name,
            container=self.container,
            release=self.release,
            cluster=self.cluster,
            exit_status=KubeCtl.pod_container_exit_status(
                container=self.container, pod=pod
            ),
            stdout=logs,
        )


@dataclass
class MwScript:
    runner: str = "/usr/local/bin/mwscript-k8s"
    kubectl_factory: Callable[[str, str], KubeCtl] = KubeCtl.from_conf

    def start(
        self, script: str, wiki: str, comment: Optional[str], *args: str
    ) -> MwScriptExecution:
        runner_args = [self.runner, "--output", "json"]
        if comment:
            runner_args.extend(["--comment", comment])
        runner_args.extend(["--", script, "--wiki", wiki])
        runner_args.extend(args)

        logger.debug("running %s", " ".join(runner_args))
        result = subprocess.run(
            runner_args,
            capture_output=True,
            # This ensures ctrl-c to the python process doesn't signal the child
            start_new_session=True,
        )
        if result.returncode != 0:
            debug_print_completed_process(result)
            raise MwScriptException(
                f"{self.runner} with {script} invocation failed, exit code {result.returncode}: "
                f"{result.stderr.decode('utf-8')}"
            )
        output = json.loads(result.stdout.decode("utf8"))

        logger.debug("mwscript-k8s output: %s", output)
        if output["error"] is not None:
            debug_print_completed_process(result)
            raise MwScriptException(
                f"{self.runner} with {script} invocation failed with {output['error']}"
            )

        mwscript_env = output["mwscript"]
        return MwScriptExecution(
            kubectl=self.kubectl_factory(
                mwscript_env["config"], mwscript_env["namespace"]
            ),
            kubectl_deploy=self.kubectl_factory(
                mwscript_env["deploy_config"], mwscript_env["namespace"]
            ),
            job=mwscript_env["job"],
            container=mwscript_env["mediawiki_container"],
            release=mwscript_env["release"],
            cluster=mwscript_env["cluster"],
        )

    def run(
        self,
        script: str,
        wiki: str,
        args: List[str],
        comment: Optional[str] = None,
        capture_stdout: bool = False,
        stdout: Optional[TextIO] = None,
        check: bool = False,
    ) -> MwScriptExecutionResult:
        result = self.start(script, wiki, comment, *args).follow(
            capture_stdout=capture_stdout, stdout=stdout
        )
        if check and result.exit_status != 0:
            raise MwScriptException(
                f"{self.runner} --wiki {wiki} {' '.join(args)} failed with status code {result.exit_status}"
            )
        return result
