# Orchestrates reindexing the production cirrussearch clusters
#
# Ties together the various pieces and executes the main
# loop of the reindexing operation.

from __future__ import annotations

import signal
import threading
import time
from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path
from types import FrameType
from typing import Dict, List, Optional

from cirrus_reindexer import sup
from cirrus_reindexer.cirrus import (
    CirrusIndexAlias,
    ReindexResult,
    cancel_reindex,
    running_reindexes,
)
from cirrus_reindexer.state import ReindexingState
from cirrus_reindexer.work_queue import Task, WorkQueue

STOP = threading.Event()


class SignalHandler:
    state: ReindexingState
    work_queue: WorkQueue[ReindexTask]
    repeat_signal_timeout_sec: float
    next_signal_before: Optional[float]

    def __init__(
        self,
        state: ReindexingState,
        work_queue: WorkQueue[ReindexTask],
        repeat_signal_timeout_sec: float = 5.0,
    ):
        self.state = state
        self.work_queue = work_queue
        self.repeat_signal_timeout_sec = repeat_signal_timeout_sec
        self.next_signal_before = None

    def install(self) -> None:
        signal.signal(signal.SIGINT, self)

    def __call__(self, signal: int, frame: Optional[FrameType]) -> None:
        print("\nReceived shutdown request via Ctrl-C")
        if not STOP.is_set():
            STOP.set()
            self.work_queue.shutdown(wait=False)
            print(
                "No more reindex operations will be started.\n"
                "In-progress reindexing and backfill operations will not be stopped.\n"
            )

        now = time.monotonic()
        if not self.next_signal_before or now > self.next_signal_before:
            self.next_signal_before = now + self.repeat_signal_timeout_sec
            print(
                f"Press Ctrl-C again within {self.repeat_signal_timeout_sec}s to cancel reindex tasks.\n"
                "ALL reindex tasks across all known clusters will be canceled, even if not issued by us.\n",
            )
            return

        # By cancelling the reindex task in elasticsearch, instead of directly
        # stopping the php scripts, we allow cirrus to notice that the reindex
        # failed and clean up after itself.
        print(
            "Cancelling in-progress reindex operations. Reindex monitor needs up to 30s to notice.\n"
            "Backfill task will not be stopped."
        )
        base_urls = {
            cirrus_index.base_url for cirrus_index in self.state.aliases.values()
        }
        for base_url in base_urls:
            for task_id, source_index_name in running_reindexes(base_url).items():
                print("Cancelling reindex of {source_index_name} on {base_url}")
                cancel_reindex(base_url, task_id)
        print("")


@dataclass(frozen=True)
class ReindexTask(Task):
    cirrus_index: CirrusIndexAlias
    state: ReindexingState
    log_path: Path
    force_reindex: bool

    @property
    def weight(self) -> int:
        return self.cirrus_index.shard_count

    @property
    def alias_name(self) -> str:
        return self.cirrus_index.alias_name

    def __str__(self) -> str:
        return f"<ReindexTask(alias_name={self.alias_name}, weight={self.weight})>"

    def __call__(self) -> bool:
        # Start the reindex and let it run. We don't care that much about the output,
        # we will decide if it worked based on if the live indices change.
        self.state.reindex_started(self.cirrus_index)
        result = None
        try:
            with self.log_path.open("at") as f:
                result = self.cirrus_index.reindex(self.force_reindex, stdout=f)
        finally:
            self.state.reindex_completed(
                self.cirrus_index, result is ReindexResult.UNNECESSARY
            )
        return self.state.is_reindexing_completed(self.cirrus_index)


def wait_for_completion(
    reindex_queue: WorkQueue[ReindexTask],
    backfiller: sup.Backfiller,
    max_reindex_retries: int,
) -> List[ReindexTask]:
    retries: Dict[str, int] = defaultdict(int)
    failed: List[ReindexTask] = []
    try:
        backfiller.start()
        for task, success in reindex_queue.run():
            if not backfiller.is_alive():
                print("WARNING: Backfiller is not alive. Restarting.")
                backfiller = backfiller.clone()
                backfiller.start()
            if success:
                # Critically this only refers to the subprocess return code, If
                # reindexing is complete is determined by the live indices changing.
                print(f"{task.alias_name} completed successfully.")
                continue
            if retries[task.alias_name] >= max_reindex_retries:
                print(f"WARNING: Exceeded max retries on {task.alias_name}. Giving up.")
                failed.append(task)
            else:
                print(
                    f"Fail.cirrus_indexed reindex on {task.alias_name}. Trying again."
                )
                retries[task.alias_name] += 1
                reindex_queue.add(task)
            if STOP.is_set():
                break
    finally:
        print("\nShutting down work queue.")
        print("Waiting for any in-progress reindex threads to complete...")
        for task, success in reindex_queue.shutdown(wait=True):
            if not success:
                failed.append(task)
                print(f"WARNING: Failed reindexing {task.alias_name}. Giving up.")
        print("\nAll in-flight reindex operations completed. Waiting for backfill")
        backfiller.shutdown()
        backfiller.join()
        return failed
