from __future__ import annotations

import os
import subprocess
import threading
from datetime import datetime, timedelta
from pathlib import Path
from textwrap import dedent
from typing import Collection, Iterator, Optional

import yaml

from cirrus_reindexer.flink import flink_status, wait_for_flink_status
from cirrus_reindexer.k8s import KubectlException, Release
from cirrus_reindexer.state import BackfillRequest, ReindexingState
from cirrus_reindexer.utils import debug_print_completed_process, optional_open


def backfill_release_for_cluster(replica: str) -> Release:
    if replica == "cloudelastic":
        return Release(
            "cirrus-streaming-updater", "eqiad", "consumer-cloudelastic-backfill"
        )
    return Release("cirrus-streaming-updater", replica, "consumer-search-backfill")


def helmfile_backfill(
    release: Release, *args: str, log_path: Optional[Path]
) -> subprocess.CompletedProcess:
    with optional_open(log_path, "at") as f:
        return release.helmfile(
            "--state-values-set",
            "backfill=true",
            *args,
            cwd=os.environ.get(
                "SUP_HELMFILE_PATH",
                "/srv/deployment-charts/helmfile.d/services/cirrus-streaming-updater",
            ),
            stdout=f,
            stderr=f,
        )


def require_backfill_not_deployed(release: Release) -> bool:
    # We could accept FINISHED or FAILED here, but if we are expecting those
    # statuses to result in a `helmfile destroy` then our deploy could race
    # with an external destroy attempt. Better to let the operator resolve.
    initial_status = flink_status(release)
    if initial_status == "NOT_DEPLOYED":
        return True

    print(
        dedent(
            f"""
        ERROR: backfill release is already deployed. Cannot backfill.
        Current status is: {initial_status}

        To wait for a still running backfill:

            kube_env {release.namespace} {release.cluster}
            kubectl get -o json --selector release={release.name} flinkdeployment | jq .items[0].status.jobStatus.state'

        To manually destroy the backfill:

            cd /srv/deployment/helmfile.d/services/cirrus-streaming-updater
            kube_env {release.namespace} {release.cluster}
            helmfile -i -e {release.cluster} --selector name={release.name} --state-values-set backfill=true destroy
    """
        )
    )
    return False


APP_CONFIG_PATH = "app.config_files.app\\.config\\.yaml"
START_TIME = "kafka-source-start-time"
END_TIME = "kafka-source-end-time"
WIKIIDS = "wikiids"
INDEX_ALIASES = "index-aliases-faux-param"


def deploy_backfill(
    release: Release,
    dbnames: Collection[str],
    start_at: datetime,
    end_at: datetime,
    log_path: Optional[Path],
    index_aliases: Optional[Iterator[str]] = None,
) -> bool:
    args = [
        "apply",
        "--context",
        "5",
        "--set",
        f"{APP_CONFIG_PATH}.{START_TIME}={start_at.isoformat()}Z",
        "--set",
        f"{APP_CONFIG_PATH}.{END_TIME}={end_at.isoformat()}Z",
        "--set",
        f'{APP_CONFIG_PATH}.{WIKIIDS}={";".join(dbnames)}',
    ]
    if index_aliases is not None:
        # This isn't a real config item for SUP, but SUP ignores extra bits and
        # we need this in detect_backfill_request to know which index was being
        # backfilled.
        args.extend(
            [
                "--set",
                f'{APP_CONFIG_PATH}.{INDEX_ALIASES}={";".join(index_aliases)}',
            ]
        )
    result = helmfile_backfill(release, *args, log_path=log_path)
    if result.returncode != 0:
        print("WARNING: Failed to apply backfill release")
        debug_print_completed_process(result)
        return False
    return True


def wait_for_backfill(release: Release, log_path: Optional[Path]) -> bool:
    # NOT_DEPLOYED happens when an operator deletes the release.
    # Not sure when CANCELED happens, but it probably means stop.
    final_status = wait_for_flink_status(
        release, {"CANCELED", "NOT_DEPLOYED", "FAILED", "FINISHED"}, progress=False
    )
    print(f"Final backfill status: {final_status}")

    result = helmfile_backfill(release, "destroy", log_path=log_path)
    if result.returncode != 0:
        print("WARNING: Failed to destroy backfill release")
        debug_print_completed_process(result)
        return False

    # Wait for the release to fully undeploy, so the next reindex doesn't
    # have opportunity to think something is running as long as it waits
    # for this function to exit.
    wait_for_flink_status(release, {"NOT_DEPLOYED"})

    return final_status == "FINISHED"


def backfill(
    release: Release,
    dbnames: Collection[str],
    start_at: datetime,
    end_at: datetime,
    log_path: Optional[Path],
) -> bool:
    print(
        f"Starting backfill on {len(dbnames)} wiki(s) for "
        f"{start_at.isoformat()} to {end_at.isoformat()}"
    )
    # TODO: The printed error instructions are a bit odd now that
    # we can pickup a deployed backfill and complete it. We need
    # to re-think how backfills are handled in helmfile. This should
    # be rare for the same reason though.
    if not require_backfill_not_deployed(release):
        return False
    # This could race with a deploy. But probably not.
    if not deploy_backfill(release, dbnames, start_at, end_at, log_path):
        return False
    return wait_for_backfill(release, log_path)


def detect_backfill_request(release: Release) -> BackfillRequest:
    cm = release.find_configmap("flink-app-config")
    app_config = yaml.safe_load(cm.data["app.config.yaml"])

    try:
        index_aliases = app_config[INDEX_ALIASES].split(";")
    except KeyError:
        raise ValueError("Unrecognized backfill not issued by orchestration")

    # [:-1] strips the trailing Z, which python <= 3.10 rejects.
    start_at = datetime.fromisoformat(app_config[START_TIME][:-1])
    end_at = datetime.fromisoformat(app_config[END_TIME][:-1])
    return BackfillRequest.of(index_aliases, start_at, end_at)


class Backfiller(threading.Thread):
    SEQUENTIAL_FAILURE_LIMIT = 5

    state: ReindexingState
    release: Release
    max_backfill_delay: timedelta
    shutdown_requested: threading.Event
    next_log_path: Iterator[Path]
    sequential_failures: int

    def __init__(
        self,
        state: ReindexingState,
        release: Release,
        next_log_path: Iterator[Path],
    ):
        super().__init__(name="backfiller")
        self.state = state
        self.release = release
        self.shutdown_requested = threading.Event()
        self.next_log_path = next_log_path
        self.sequential_failures = 0

    def clone(self) -> Backfiller:
        clone = Backfiller(self.state, self.release, self.next_log_path)
        clone.shutdown_requested = self.shutdown_requested
        return clone

    def shutdown(self) -> None:
        """Shutdown thread after all backfills are complete"""
        self.shutdown_requested.set()

    def run(self) -> None:
        if self.shutdown_requested.is_set():
            return
        self._wait_for_running_backfill()
        while True:
            backfill_request = self.state.pending_backfill()
            if backfill_request:
                self._backfill(backfill_request)
            elif self.shutdown_requested.wait(10):
                break

        # Finish any backfill that was registered while we were waiting
        # for the shutdown signal
        backfill_request = self.state.pending_backfill()
        if backfill_request:
            self._backfill(backfill_request)

    def _backfill(self, request: BackfillRequest) -> None:
        dbnames = {
            self.state.aliases[index_alias].dbname
            for index_alias in request.index_aliases
        }
        if backfill(
            self.release,
            dbnames,
            request.start_at,
            request.end_at,
            next(self.next_log_path),
        ):
            self.sequential_failures = 0
            self.state.backfill_completed(request)
        else:
            self.sequential_failures += 1
            if self.sequential_failures >= self.SEQUENTIAL_FAILURE_LIMIT:
                raise RuntimeError(
                    f"Backfill failed {self.sequential_failures} times in a row."
                )
            print("Backfill failed. Will try again")

    def _wait_for_running_backfill(self) -> None:
        """Handles a backfill already deployed on startup.

        Assumes that whatever was managing the backfill previously has died,
        and will perform the normal post-backfill operations when it completes.
        """
        try:
            request = detect_backfill_request(self.release)
        except KubectlException:
            # Not running. Hopefully.
            return
        print(
            f"Recovered backfill on {len(request)} wiki(s) for "
            f"{request.start_at.isoformat()} to {request.end_at.isoformat()}"
        )
        if wait_for_backfill(self.release, next(self.next_log_path)):
            self.state.backfill_completed(request)
        else:
            print("Backfill release that was already running on startup failed")
