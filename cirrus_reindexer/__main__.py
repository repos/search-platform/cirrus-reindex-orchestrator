#!/usr/bin/python3
# Automates reindex and backfill operations for simple single-wiki operations
import logging
import sys
from argparse import ArgumentParser
from datetime import datetime
from typing import List

from cirrus_reindexer import sup
from cirrus_reindexer.utils import expand_with_expanddblist


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser(description="Cirrus backfiller")
    parser.add_argument(
        "cirrus_cluster", choices=["eqiad", "codfw", "cloudelastic"], dest="replica"
    )
    parser.add_argument(
        "--dbnames",
        nargs="*",
        default=["@all"],
        help="The set of wikis to backfill. Values prefixed with @ will be "
        "expanded with expanddblist. Defaults to @all.",
    )
    parser.add_argument(
        "--exclude-dbnames",
        nargs="*",
        default=[],
        dest="excluded",
        help="The set of wikis to exclude from backfilling. "
        "Values prefixed with @ will be expanded with expanddblist. Defauls to empty list.",
    )
    parser.add_argument("start", type=datetime.fromisoformat, required=True)
    parser.add_argument("end", type=datetime.fromisoformat, required=True)
    parser.add_argument("--verbose", action="store_true")

    return parser


def main(
    replica: str,
    dbnames: List[str],
    excluded: List[str],
    start: datetime,
    end: datetime,
    verbose: bool,
) -> int:
    if verbose:
        logging.basicConfig(
            format="%(levelname)s - %(asctime)s:%(message)s", level=logging.DEBUG
        )
    release = sup.backfill_release_for_cluster(replica)
    if not sup.require_backfill_not_deployed(release):
        return 1

    resolved = expand_with_expanddblist(dbnames, excluded)
    success = sup.backfill(release, resolved, start, end, log_path=None)

    return 0 if success else 1


def entry_point() -> int:
    args = arg_parser().parse_args()
    return main(**dict(vars(args)))


if __name__ == "__main__":
    sys.exit(entry_point())
