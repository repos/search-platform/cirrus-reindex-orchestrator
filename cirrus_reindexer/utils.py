import shlex
import subprocess
from contextlib import nullcontext
from io import TextIOWrapper
from pathlib import Path
from typing import ContextManager, Iterable, Optional, Set, cast


def debug_print_completed_process(result: subprocess.CompletedProcess) -> None:
    print("Args: " + " ".join(shlex.quote(x) for x in result.args))
    print(f"Return Code: {result.returncode}")
    if result.stdout is not None:
        print("--- stdout ---")
        print(result.stdout)
    if result.stderr is not None:
        print("--- stderr --")
        print(result.stderr)


def expanddblist(name: str) -> Set[str]:
    return {
        line.strip()
        for line in subprocess.check_output(["/usr/local/bin/expanddblist", name])
        .decode("utf8")
        .split("\n")
        if line.strip() != ""
    }


def expand_with_expanddblist(
    values: Iterable[str], exclude: Iterable[str] = [], verify: bool = True
) -> Set[str]:
    resolved = set()
    for value in values:
        if value[0] == "@":
            resolved.update(expanddblist(value[1:]))
        else:
            resolved.add(value)
    if exclude:
        resolved -= expand_with_expanddblist(exclude, verify=verify)
    if verify:
        unknown = resolved - expanddblist("all")
        if unknown:
            raise ValueError(f"Unknown dbnames: {unknown}")
    return resolved


def optional_open(
    path: Optional[Path],
    mode: str,
) -> ContextManager[Optional[TextIOWrapper]]:
    """Context manager optionally opens the log path

    Mostly exists to make mypy happy with a return type.
    Assumes file was opened in text mode and not binary.
    """
    if path:
        return cast(ContextManager[TextIOWrapper], path.open(mode))
    else:
        return nullcontext()
