from __future__ import annotations

import json
import threading
import time
from datetime import datetime
from pathlib import Path
from typing import Any, Callable, Dict, Iterable, List, Mapping, Optional, Set, Tuple

from cirrus_reindexer.cirrus import CirrusIndexAlias


class BackfillRequest:
    """Immutable model of requested backfills"""

    requests: Tuple[Tuple[str, datetime, datetime], ...]

    def __init__(
        self, requests: Optional[Iterable[Tuple[str, datetime, datetime]]] = None
    ):
        self.requests = tuple(requests) if requests else tuple()

    @classmethod
    def of(
        cls, index_aliases: Iterable[str], start_at: datetime, end_at: datetime
    ) -> BackfillRequest:
        return cls((index_alias, start_at, end_at) for index_alias in index_aliases)

    def __len__(self):
        return len(self.requests)

    def __repr__(self):
        return f"BackfillRequest<{self.index_aliases}, {self.start_at}, {self.end_at}>"

    @property
    def index_aliases(self) -> Set[str]:
        return {index_alias for index_alias, _, _ in self.requests}

    @property
    def start_at(self) -> datetime:
        return min(start_at for _, start_at, _ in self.requests)

    @property
    def end_at(self) -> datetime:
        return max(end_at for _, _, end_at in self.requests)

    @property
    def earliest_end_at(self) -> datetime:
        return min(end_at for _, _, end_at in self.requests)

    def add(
        self, index_alias: str, start_at: datetime, end_at: datetime
    ) -> BackfillRequest:
        return BackfillRequest(self.requests + ((index_alias, start_at, end_at),))

    def complete_from(self, other: BackfillRequest) -> BackfillRequest:
        # This includes the assumption that a backfill applies the same start/end
        # to all indices, even though the indices have separate requests.
        index_aliases = other.index_aliases
        start_at = other.start_at
        end_at = other.end_at
        return BackfillRequest(
            (req_index_alias, req_start_at, req_end_at)
            for req_index_alias, req_start_at, req_end_at in self.requests
            if req_index_alias not in index_aliases
            or req_start_at < start_at
            or req_end_at > end_at
        )


class ReindexingState:
    """Records and reports on the state of the global reindexing process

    Expected operation:
    * On initial setup load all aliases into reindexing state
    * On restore, verify all aliases exist in reindexing state
    * Do not add aliases after startup (might work, untested)
    * After setup, query pending_reindexes() and queue all operations
    * Invoke reindex_started before starting a reindx operation
    * If a reindexing operation fails re-queue it.
    * When a reindexing operation completes or fails call reindex_completed
    * Periodically poll pending_backfill() and run the backfill.
    * Provide same backfill object to backfill_completed on success
    * Complete when pending_reindexes() and pending_backfill() are both empty,
     and no operation is in-progress.
    """

    path: Path
    aliases: Dict[str, CirrusIndexAlias]
    # Map from index_alias to concrete index name seen on first startup
    _initial_indices: Dict[str, str]
    # Map from index_alias to concrete index name seen most recently.
    _live_indices: Dict[str, str]
    # Map from index_alias to when the reindex operation started
    _reindex_started_at: Dict[str, datetime]
    # Tracking for the set of backfills that are required
    _backfill_request: BackfillRequest
    # Set of alias names that cirrus said dont need reindexing
    _reindex_not_needed: Set[str]
    # General lock over read/write operations
    _lock: threading.Lock
    # The number of events that have been applied to the state
    _event_count: int

    def __init__(self, path: Path, clock: Callable[[], float] = time.time):
        self.path = path
        self._clock = clock
        self._lock = threading.Lock()
        self._reset()

    def __len__(self):
        return self._event_count

    # Persistence

    def reload(self) -> None:
        with self._lock:
            self._reset()
            with self.path.open(mode="rt") as f:
                for line in f:
                    self._apply(json.loads(line))

    def _append(self, event_type: str, properties: Mapping[str, Any]) -> None:
        event = {
            "dt": self._clock(),
            "type": event_type,
            **properties,
        }
        with self._lock:
            with self.path.open(mode="at") as f:
                f.write(json.dumps(event) + "\n")
            self._apply(event)

    # Runtime state management

    def _reset(self) -> None:
        self.aliases = dict()
        self._initial_indices = dict()
        self._live_indices = dict()
        self._reindex_started_at = dict()
        self._backfill_request = BackfillRequest()
        self._reindex_not_needed = set()
        self._event_count = 0

    def _apply(self, update: Mapping[str, Any]) -> None:
        assert self._lock.locked()
        self._event_count += 1
        dt = datetime.fromtimestamp(update["dt"])

        if update["type"] == "add_reindex_target":
            self.aliases[update["alias_name"]] = CirrusIndexAlias(
                update["alias_name"],
                update["dbname"],
                update["replica"],
                update["base_url"],
                update["shard_count"],
            )

        elif update["type"] == "live_index_name":
            alias_name = update["alias_name"]
            if alias_name not in self._initial_indices:
                self._initial_indices[alias_name] = update["index_name"]
            self._live_indices[alias_name] = update["index_name"]

        elif update["type"] == "reindex_started":
            alias_name = update["alias_name"]
            if alias_name in self._reindex_started_at:
                # Suspect this could happen if the script dies while a reindex
                # is running. We might want to be more explicit about handling
                # these cases? Maybe on startup check cluster for running
                # reindexes and cancel them? Punting until it's a real issue.
                print("WARNING: Starting a reindex without completing previous attempt")
            if (
                alias_name not in self._reindex_started_at
                or dt < self._reindex_started_at[alias_name]
            ):
                self._reindex_started_at[alias_name] = dt

        elif update["type"] == "reindex_completed":
            alias_name = update["alias_name"]
            try:
                dt_before = self._reindex_started_at.pop(alias_name)
            except KeyError:
                raise ValueError(
                    f"Trying to complete a reindex that wasn't started: {alias_name}"
                )
            before = self._initial_indices[alias_name]
            after = self._live_indices[alias_name]
            if before != after:
                self._backfill_request = self._backfill_request.add(
                    alias_name, dt_before, dt
                )
            elif update["reindex_is_unnecessary"]:
                print(f"Cirrus refused to reindex {alias_name} as nothing would change")
                self._reindex_not_needed.add(alias_name)
            else:
                print(
                    f"Reindex of {alias_name} did not change live index. Skipping backfill."
                )

        elif update["type"] == "backfill_completed":
            request = BackfillRequest.of(
                update["index_aliases"],
                datetime.fromtimestamp(update["start_at"]),
                datetime.fromtimestamp(update["end_at"]),
            )
            self._backfill_request = self._backfill_request.complete_from(request)

        else:
            raise NotImplementedError(f'Unknown update type: {update["type"]}')

    # State transitions

    def _record_live_index_name(self, cirrus_index: CirrusIndexAlias) -> None:
        self._append(
            "live_index_name",
            {
                "alias_name": cirrus_index.alias_name,
                "index_name": cirrus_index.live_index_name(),
            },
        )

    def add_reindex_target(self, cirrus_index: CirrusIndexAlias) -> None:
        self._append(
            "add_reindex_target",
            {
                "alias_name": cirrus_index.alias_name,
                "dbname": cirrus_index.dbname,
                "replica": cirrus_index.replica,
                "base_url": cirrus_index.base_url,
                "shard_count": cirrus_index.shard_count,
            },
        )
        self._record_live_index_name(cirrus_index)

    def reindex_started(self, cirrus_index: CirrusIndexAlias) -> None:
        self._append("reindex_started", {"alias_name": cirrus_index.alias_name})

    def reindex_completed(
        self, cirrus_index: CirrusIndexAlias, reindex_is_unnecessary: bool
    ) -> None:
        # Note that this may or may not be a succesfull completion,
        # that will be determined based on the recorded indices
        self._record_live_index_name(cirrus_index)
        self._append(
            "reindex_completed",
            {
                "alias_name": cirrus_index.alias_name,
                # But that can be overridden explicitly for cases where cirrus knows
                # that a reindex would have no effect.
                "reindex_is_unnecessary": reindex_is_unnecessary,
            },
        )

    def backfill_completed(self, backfill_request: BackfillRequest) -> None:
        self._append(
            "backfill_completed",
            {
                "index_aliases": list(backfill_request.index_aliases),
                "start_at": backfill_request.start_at.timestamp(),
                "end_at": backfill_request.end_at.timestamp(),
            },
        )

    # State queries

    def pending_reindex(self) -> List[CirrusIndexAlias]:
        """Reports the list of aliases that need to be reindexed

        Expects that the live indices are recorded when the index is
        first added, and then again any time a reindexing operation
        completes.

        Does not take into account reindex's that are currently in-flight.
        """
        # Not locking because it isn't reentrant, and we only add aliases
        # during startup.
        return [
            index_alias
            for index_alias in self.aliases.values()
            if not self.is_reindexing_completed(index_alias)
        ]

    def is_reindexing_completed(self, cirrus_index: CirrusIndexAlias) -> bool:
        with self._lock:
            if cirrus_index.alias_name in self._reindex_not_needed:
                return True
            try:
                initial = self._initial_indices[cirrus_index.alias_name]
                current = self._live_indices[cirrus_index.alias_name]
            except KeyError:
                return False
            return initial != current

    def pending_backfill(self) -> BackfillRequest:
        """Reports backfills that are still required"""
        with self._lock:
            return self._backfill_request
