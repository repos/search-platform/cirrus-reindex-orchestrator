import json
import time
from typing import Set

from cirrus_reindexer.k8s import KubectlException, Release


def flink_status_via_rest(release: Release) -> str:
    """Fetch the status of a flink release from flink rest api

    This is very unreliable, but covers an important edge case that would
    otherwise leave the backfill running but doing nothing.
    """
    pod = release.find_pod({"component": "jobmanager"})
    res = release.kubectl_deploy.exec(
        pod,
        "flink-main-container",
        "python3",
        "-c",
        """
import urllib.request;
print(urllib.request.urlopen("http://localhost:8081/v1/jobs").read().decode("utf8"))
""",
    )
    output = json.loads(res)
    if "jobs" not in output:
        # Probably also not that rare
        raise KubectlException("Bad api response from flink /v1/jobs")
    return str(output["jobs"][0]["status"])


def flink_status(release: Release) -> str:
    # Querying via kubectl is generally more reliable for a wide variety
    # of circumstances. Unfortunately it will, in rare cases, fail to update
    # from RUNNING to FINISHED. In rare cases we have to query the rest api
    # to know when it's actually done.
    for remaining in reversed(range(0, 30)):
        status = release.get_flink_deployment_status()
        if status != "RUNNING":
            return status
        try:
            return flink_status_via_rest(release)
        except KubectlException as e:
            # flink_status_via_rest is very unreliable, particularly
            # during shutdown. Simply ignore it's failures and check
            # the normal way if we are shutdown.
            print(
                "Backfill is currently running. "
                "Failed to fetch status via rest. "
                f"{remaining} attempts remaining"
            )
            print(e)
            time.sleep(1)
    # Return UNKNOWN?
    raise KubectlException("Failed to fetch flink status")


def wait_for_flink_status(
    release: Release, allowed: Set[str], progress: bool = True
) -> str:
    print(
        f"Waiting for {release} to reach one of {allowed}",
        end="..." if progress else "\n",
        flush=True,
    )
    while True:
        status = flink_status(release)
        if status in allowed:
            progress and print("Done", flush=True)
            return status
        time.sleep(10)
        progress and print(".", end="", flush=True)
