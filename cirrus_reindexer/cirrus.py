from __future__ import annotations

import json
import re
from dataclasses import dataclass, field
from enum import Enum
from typing import (
    Callable,
    Iterable,
    Iterator,
    List,
    Mapping,
    MutableMapping,
    TextIO,
    cast,
)

import requests
from cirrus_toolbox.mw_api_client import MWClient
from cirrus_toolbox.noc_client import NOCClient

from cirrus_reindexer.mwscript_k8s import MwScript

# The cirrussearch index types we recognize. These are the set
# of indices that represent mediawiki pages. There is also
# archive and titlesuggest, but they have different processes.
ACCEPTED_ALIAS_SUFFIXES = {"content", "general", "file"}


class CirrusConnectionInfo:
    def __init__(self, mwscript_runner: MwScript = MwScript()):
        # map of group -> map of cluster -> connection URL, e.g.:
        # { "chi": {"eqiad": "https://...", "codfw': "https://" }, "psi": {...} }
        self.cached: MutableMapping[str, MutableMapping[str, str]] = {}
        self.mwscript_runner: MwScript = mwscript_runner

    def get_connection_info(self, dbname: str, cluster: str, group: str) -> str:
        if group not in self.cached.keys() or cluster not in self.cached[group].keys():
            self._populate_cache_from_expected_indices(dbname)
        return self.cached[group][cluster]

    def _populate_cache_from_expected_indices(self, dbname: str) -> None:
        exec_result = self.mwscript_runner.run(
            "extensions/CirrusSearch/maintenance/ExpectedIndices.php",
            dbname,
            [],
            comment="cirrus-reindex-orchestrator",
            check=True,
            capture_stdout=True,
        )
        if exec_result.stdout is None:
            raise RuntimeError("Expected mwscript to return an output")
        result: Mapping = json.loads(exec_result.stdout)
        for replica, info in result["clusters"].items():
            group: str = info["group"]
            if group not in self.cached:
                self.cached[group] = {}
            self.cached[group][replica] = CirrusConnectionInfo._wiki_base_url(info)
        exec_result.cleanup()

    @staticmethod
    def _wiki_base_url(clusters_entry: Mapping) -> str:
        """Get base_url from entry of ExpectedIndices.php output clusters dict"""
        conn = clusters_entry["connection"][0]
        scheme = "http" if conn["transport"].endswith("Http") else "https"
        return f'{scheme}://{conn["host"]}:{conn["port"]}'


def elastic_cat(base_url: str, path: str) -> List[Mapping[str, str]]:
    response = requests.get(
        f"{base_url}/_cat/{path}", headers={"Accept": "application/json"}
    )
    response.raise_for_status()
    return cast(List[Mapping[str, str]], response.json())


def get_live_indices(base_url: str, alias: str) -> List[str]:
    found = elastic_cat(base_url, f"aliases/{alias}")
    if not found:
        raise ValueError(f"Alias ({alias}) does not exist on {base_url}")
    return [line["index"] for line in found]


def export_wiki_config(
    dbname: str, noc_client: NOCClient, mw_client_factory: Callable[[str], MWClient]
) -> Mapping:
    hostname = noc_client.get_hostname_by_wiki_id(dbname)
    mwclient = mw_client_factory(hostname)
    return mwclient.expected_indices()


class ReindexResult(Enum):
    ERROR = "error"
    SUCCESS = "success"
    UNNECESSARY = "unnecessary"


def reindex_one_search_index(
    replica: str,
    wiki: str,
    index_suffix: str,
    force: bool,
    mwscript_runner: MwScript,
    stdout: TextIO,
) -> ReindexResult:
    desc = f'reindex of {wiki} {index_suffix} on the {replica} cluster"'
    print(f"Starting {desc}")
    args = [
        f"--cluster={replica}",
        "--reindexAndRemoveOk",
        "--indexIdentifier=now",
        f"--indexSuffix={index_suffix}",
    ]
    if force:
        args.append("--ignoreIndexChanged")
    exec_result = mwscript_runner.run(
        "extensions/CirrusSearch/maintenance/UpdateOneSearchIndexConfig.php",
        wiki,
        args,
        comment="cirrus-reindex-orchestrator",
        capture_stdout=False,
        stdout=stdout,
        check=False,
    )

    print(f"Completed {desc} with return code {exec_result.exit_status}")
    exec_result.cleanup()
    if exec_result.exit_status == 0:
        return ReindexResult.SUCCESS
    elif exec_result.exit_status == 10:
        return ReindexResult.UNNECESSARY
    else:
        return ReindexResult.ERROR


# Pattern matches [bracketed] parts of a string.
BRACKETED_RE = re.compile(r"\[([^\]]+)\]")


def _source_index_from_reindex_desc(desc: str) -> str:
    # Ex: Reindex from [testwiki_content] to [testwiki_content_12345][_doc]
    re_match = BRACKETED_RE.search(desc)
    if not re_match:
        raise ValueError(f"Unexpected reindex description: {desc}")
    return re_match.group(1)


def running_reindexes(base_url: str) -> Mapping[str, str]:
    """List reindexes running on the provided elastic cluster

    Result is a map from task_id to the name of the source index.
    """
    res = requests.get(f"{base_url}/_tasks?detailed=true")
    res.raise_for_status()
    return {
        task_id: _source_index_from_reindex_desc(task["description"])
        for node in res.json()["nodes"].values()
        for task_id, task in node["tasks"].items()
        if task["action"] == "indices:data/write/reindex"
        and "parent_task_id" not in task
    }


def cancel_reindex(base_url: str, task_id: str) -> None:
    # Do anything with the result?
    requests.post(f"{base_url}/_tasks/{task_id}/_cancel")


@dataclass
class CirrusIndexAlias:
    """Represents a single cirrussearch index on a specific replica"""

    alias_name: str  # testwiki_content/etc.
    dbname: str  # mediawikiwiki/testwiki/etc.
    replica: str  # eqiad/codfw/cloudelastic
    base_url: str  # https://<host>:<port>/
    shard_count: int  # Proxy for how much load reindexing induces
    _mwscript_runner: MwScript = field(default_factory=lambda: MwScript())

    @classmethod
    def from_external_state(
        cls,
        dbname: str,
        replica: str,
        mwclient: MWClient,
        conn_info: CirrusConnectionInfo,
    ) -> List[CirrusIndexAlias]:
        config = mwclient.expected_indices()
        if config["dbname"] != dbname:
            raise RuntimeError(
                f"Expected {dbname} from cirrus-config-dump api but got {config['dbname']}"
            )
        try:
            base_url = conn_info.get_connection_info(
                dbname, replica, config["clusters"][replica]["group"]
            )
        except KeyError:
            raise ValueError(f"Cluster {replica} not found for {dbname}")
        return [
            CirrusIndexAlias(
                alias_name,
                dbname,
                replica,
                base_url,
                config["clusters"][replica]["shard_count"][alias_name],
            )
            for alias_name in config["clusters"][replica]["aliases"]
            if alias_name.split("_")[-1] in ACCEPTED_ALIAS_SUFFIXES
        ]

    @classmethod
    def from_external_states(
        cls,
        dbnames: Iterable[str],
        replica: str,
        noc_client: NOCClient,
        mw_client_factory: Callable[[str], MWClient],
    ) -> Iterator[CirrusIndexAlias]:
        conn_info = CirrusConnectionInfo()
        for dbname in dbnames:
            hostname = noc_client.get_hostname_by_wiki_id(dbname)
            yield from cls.from_external_state(
                dbname, replica, mw_client_factory(hostname), conn_info
            )

    @property
    def alias_suffix(self):
        return self.alias_name.split("_")[-1]

    def live_index_name(self) -> str:
        indices = get_live_indices(self.base_url, self.alias_name)
        if len(indices) > 1:
            raise ValueError(
                f"More than one index assigned to {self.alias_name}: {indices}"
            )
        return indices[0]

    def reindex(self, force_reindex: bool, stdout: TextIO) -> ReindexResult:
        return reindex_one_search_index(
            self.replica,
            self.dbname,
            self.alias_suffix,
            force_reindex,
            self._mwscript_runner,
            stdout=stdout,
        )
