# Manages reindexing the production cirrussearch clusters
#
# Goals:
# 1) Continuable reindexing process that guarantees at the end that all wikis
#    have reindexed.  We can't depend on this script not dieing over a multi-day
#    runtime.
# 2) Many wikis must be backfilled as a batch in a single operation.
# 3) If a backfill ever fails we need to re-run the same backfill. Not
#    reindex-and-backfill.
# 4) Parallelize reindexing for multiple wikis at the same time, with limits
#    based on # of shards and # of wikis running in parallel.
#
# Design:
#
# The general idea here is to track reindexing by recording the indices
# attached to prod aliases. Backfill after any change in live indices.
# Consider reindexing done when all index aliases have either changed
# since startup, or cirrus has explicitly informed us that no reindex
# operation is necessary.
#
# To meet the primary goal of a continuable process we implement a
# ReindexingState that operates on a sequence of events. These events are
# written to an append-only log as json lines so they can be reloaded if the
# script exits, and inspected or modified for debugging. This log records the
# set of wikis operated on, when indices were seen as live in the clusters,
# when reindexing started and ended, and when backfill operations completed.
# This state is queried at runtime to determine what to do next. If more
# durability is required this can transition to sqlite storing the event
# sequence, but seemed unnecessary for now.
#
# On initial setup a ReindexTask is created for each page index on each
# requested wiki.  The tasks are weighted by the index shard count and pushed
# into a queue. The queue runs tasks in parallel, filling up the available
# shard limit and parallel index limits starting with the largest indexes,
# working it's way down to the smallest ones. When a ReindexTask completes it
# adds to the BackfillRequest queue.
#
# The BackfillRequest queue is monitored by a dedicated backfilling thread.
# This thread will check for a pending request, run it, and sleep when nothing
# is available.
#
# Reindex and backfill tasks are separate and run in parallel. Many reindexing
# tasks run at the same time. Only one backfill runs at a time, but it operates
# on all pending wikis at once.
#
# Reindexing is only considered complete when the live index attached to each
# page index alias has changed, or cirrus has explicitly declared there is no
# need to reindex.  If an index fails reindexing enough times we give up and
# declare failure on that index. Retries are per-execution, re-running against
# the same state after a failure will start over on retries.
#
# Backfilling is considered a success only if the flink release for backfilling
# completes succesfully.

from __future__ import annotations

import argparse
import logging
import sys
from concurrent.futures import ThreadPoolExecutor
from itertools import count
from pathlib import Path
from typing import Any, Dict, List, Optional, Set

from cirrus_toolbox.mw_api_client import (
    DEFAULT_AUTH_TOKEN_LOCATOR,
    DEFAULT_MW_ENDPOINT,
    MWClient,
)
from cirrus_toolbox.noc_client import DEFAULT_NOC_ENDPOINT, NOCClient

from cirrus_reindexer import sup
from cirrus_reindexer.cirrus import CirrusIndexAlias
from cirrus_reindexer.orchestrator import (
    ReindexTask,
    SignalHandler,
    wait_for_completion,
)
from cirrus_reindexer.state import ReindexingState
from cirrus_reindexer.utils import expand_with_expanddblist
from cirrus_reindexer.work_queue import WorkQueue

DEFAULT_UA = "cirrus_reindex_orchestrator/WMF"


def arg_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--cirrus-cluster",
        required=True,
        dest="replica",
        choices=["eqiad", "codfw", "cloudelastic"],
        help="The cirrus cluster to reindex",
    )
    parser.add_argument(
        "--shard-capacity",
        default=32,
        help="The maximum number of shards to reindex in parallel. Defaults to 32.",
    )
    parser.add_argument(
        "--max-parallel-reindex",
        default=8,
        type=int,
        help="The maximum number of wikis to reindex in parallel. Defaults to 8.",
    )
    parser.add_argument(
        "--max-reindex-retries",
        default=3,
        type=int,
        help="The maximum number of times to try reindexing a single index. Defaults to 3.",
    )
    parser.add_argument(
        "--log-dir",
        type=Path,
        default=None,
        help="The directory to write logs to. Defaults to ./{cirrus-cluster}/.",
    )
    parser.add_argument(
        "--state-path",
        type=Path,
        help="The path to store state in. Defaults to {log-dir}/state.json",
    )
    parser.add_argument(
        "--dbnames",
        nargs="*",
        default=["@all"],
        help="The set of wikis to reindex. Values prefixed with @ will be expanded with expanddblist. Defaults to @all",
    )
    parser.add_argument(
        "--exclude-dbnames",
        nargs="*",
        default=[],
        dest="excluded",
        help="The set of wikis to exclude from reindexing. "
        "Values prefixed with @ will be expanded with expanddblist. Defaults to empty list.",
    )
    parser.add_argument(
        "--mw-endpoint", default=DEFAULT_MW_ENDPOINT, help="The MW enpoint to use"
    )
    parser.add_argument(
        "--mediawiki-authorization-locator",
        default=DEFAULT_AUTH_TOKEN_LOCATOR,
        help="Defines where to source the authorization token needed to make "
        "api requests to private wikis. Contains two parts split by a :. The first "
        "half is a path to a yaml file, the second is a path within the yaml "
        "file delimited by /",
    )
    parser.add_argument(
        "--user-agent",
        default=DEFAULT_UA,
        help="The user-agent to use when making HTTP requests",
    )
    parser.add_argument(
        "--noc-endpoint", default=DEFAULT_NOC_ENDPOINT, help="The NOC endpoint to use"
    )
    parser.add_argument(
        "--force-reindex",
        action="store_true",
        help="Force reindex the wikis even if there are no mapping/analysis changes required.",
    )
    parser.add_argument("--verbose", action="store_true")
    return parser


def post_process_args(kwargs: Dict[str, Any]) -> Dict:
    # Mostly exists to resolve optional values and
    # convert types into their final form.
    excluded: List[str] = kwargs.pop("excluded")
    if kwargs["replica"] == "cloudelastic":
        print("Running on cloudelastic. Private wikis will be excluded")
        excluded.append("@private")
    kwargs["dbnames"] = expand_with_expanddblist(kwargs["dbnames"], excluded)

    log_dir: Path = kwargs.pop("log_dir")
    if log_dir is None:
        log_dir = Path(".") / kwargs["replica"]
    log_dir.mkdir(parents=True, exist_ok=True)
    kwargs["log_dir"] = log_dir

    state_path: Optional[Path] = kwargs.pop("state_path")
    if state_path is None:
        state_path = kwargs["log_dir"] / "state.json"
    print(f"Reindexing state stored in: {state_path}")
    state = ReindexingState(state_path)
    kwargs["state"] = state

    try:
        state.reload()
        print("Loaded pre-existing state")
        if any(
            cirrus_index.replica != kwargs["replica"]
            for cirrus_index in state.aliases.values()
        ):
            raise ValueError(f"Loaded state is not for replica {kwargs['replica']}")
        if any(
            cirrus_index.dbname in excluded for cirrus_index in state.aliases.values()
        ):
            raise ValueError("Loaded state contains excluded wikis")
    except FileNotFoundError:
        print("No state found.")

    return kwargs


def main(
    state: ReindexingState,
    log_dir: Path,
    replica: str,
    shard_capacity: int,
    max_parallel_reindex: int,
    max_reindex_retries: int,
    noc_endpoint: str,
    mw_endpoint: str,
    mediawiki_authorization_locator: str,
    user_agent: str,
    dbnames: Set[str],
    force_reindex: bool,
    verbose: bool,
) -> int:
    if verbose:
        logging.basicConfig(
            format="%(levelname)s - %(asctime)s:%(message)s", level=logging.DEBUG
        )

    session = MWClient.make_session()
    session.headers.update({"User-Agent": user_agent})
    noc_client = NOCClient(session, noc_endpoint)

    def mw_client_factory(hostname: str) -> MWClient:
        return MWClient.build(
            session, mw_endpoint, hostname, mediawiki_authorization_locator
        )

    executor = ThreadPoolExecutor(max_workers=max_parallel_reindex)
    backfiller = sup.Backfiller(
        state,
        sup.backfill_release_for_cluster(replica),
        # TODO: This should skip files that already exist? It currently starts
        # at 0 after a restart and appends to existing logs.
        (log_dir / f"backfill.{i:03}.log" for i in count()),
    )
    reindex_queue: WorkQueue[ReindexTask] = WorkQueue(
        executor, shard_capacity, max_parallel_reindex
    )
    signal_handler = SignalHandler(state, reindex_queue)
    signal_handler.install()

    failed_tasks: Optional[List[ReindexTask]] = None
    try:
        # TODO: We check per-wiki, but state change isn't atomic per-wiki,
        # so there is potential to miss an index under some error conditions.
        known = {cirrus_index.dbname for cirrus_index in state.aliases.values()}
        missing = dbnames - known
        for index_alias in CirrusIndexAlias.from_external_states(
            missing, replica, noc_client, mw_client_factory
        ):
            print(f"{index_alias.alias_name:>42} : initialized")
            state.add_reindex_target(index_alias)
        for index_alias in state.pending_reindex():
            task = ReindexTask(
                index_alias,
                state,
                log_dir / f"{index_alias.alias_name}.reindex.log",
                force_reindex,
            )
            reindex_queue.add(task)
        failed_tasks = wait_for_completion(
            reindex_queue, backfiller, max_reindex_retries
        )
    finally:
        if failed_tasks is None:
            print("Execution ended early. Process is likely incomplete.")
        elif failed_tasks:
            print("Failed reindex against:")
            for task in failed_tasks:
                print("\t" + task.cirrus_index.alias_name)

        pending = state.pending_reindex()
        if pending:
            print(f"Reindex is incomplete. {len(pending)} indices remain.")
        elif failed_tasks == []:
            print(
                f"Reindex operation is complete. Reindexed {len(state.aliases)} indices."
            )

    return 0


def entry_point() -> int:
    args = arg_parser().parse_args()
    return main(**post_process_args(dict(vars(args))))


if __name__ == "__main__":
    sys.exit(entry_point())
