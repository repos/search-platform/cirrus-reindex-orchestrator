from pathlib import Path

import pytest
from pytest_mock import MockerFixture

from cirrus_reindexer.cirrus import CirrusIndexAlias
from cirrus_reindexer.orchestrator import ReindexTask
from cirrus_reindexer.state import ReindexingState

DBNAME = "pytestwiki"
ALIAS_NAME = f"{DBNAME}_content"
INITIAL_LIVE_INDEX = "a"
MAX_SHARD_COUNT = 3


@pytest.fixture()
def state(mocker: MockerFixture, tmp_path: Path) -> ReindexingState:
    cirrus_index = CirrusIndexAlias(
        ALIAS_NAME, DBNAME, "dc2", "http://search-beta.dc2:9200", MAX_SHARD_COUNT
    )
    mocker.patch.object(cirrus_index, "live_index_name").return_value = (
        INITIAL_LIVE_INDEX
    )
    state = ReindexingState(tmp_path / "state.json")
    state.add_reindex_target(cirrus_index)
    cirrus_index = state.aliases[ALIAS_NAME]
    mocker.patch.object(cirrus_index, "live_index_name").return_value = (
        INITIAL_LIVE_INDEX
    )
    return state


def test_reindex_task(
    mocker: MockerFixture, state: ReindexingState, tmp_path: Path
) -> None:
    cirrus_index = state.aliases[ALIAS_NAME]
    task = ReindexTask(cirrus_index, state, tmp_path / "reindex.log", False)
    assert task.weight == 3
    assert len(state) == 2

    # Reindex completed, live indices change
    mocker.patch.object(cirrus_index, "reindex").return_value = None
    mocker.patch.object(cirrus_index, "live_index_name").return_value = "b"
    assert task()

    # Reindex completed, new events collected
    assert state.is_reindexing_completed(cirrus_index)
    assert len(state) == 5


def test_reindex_task_fails(
    mocker: MockerFixture, state: ReindexingState, tmp_path: Path
) -> None:
    cirrus_index = state.aliases[ALIAS_NAME]
    task = ReindexTask(cirrus_index, state, tmp_path / "reindex.log", False)
    assert len(state) == 2

    def fail(*args, **kwargs):
        raise AssertionError("failed")

    # Reindex fails, no change to live indices
    mocker.patch.object(cirrus_index, "reindex").side_effect = fail
    with pytest.raises(AssertionError):
        task()

    # Reindex pending, new events collected
    assert not state.is_reindexing_completed(cirrus_index)
    assert len(state) == 5
