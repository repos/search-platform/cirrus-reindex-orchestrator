import json
from collections import namedtuple
from dataclasses import dataclass
from typing import Callable, Optional
from unittest.mock import Mock

import pytest
from kubernetes.client import ApiClient, CoreV1Api, CustomObjectsApi
from kubernetes.watch import Watch
from pytest_mock import MockerFixture

from cirrus_reindexer.k8s import KubeCtl, Release


@dataclass
class KubeClientMock:
    core_v1: Mock
    custom_objects: Mock
    k8s_client: Mock
    _mocker: MockerFixture
    _websocket_stream_exec_mock: Optional[Callable] = None

    def build_client(self, ns: str) -> KubeCtl:
        return KubeCtl(
            core_v1=self.core_v1,
            custom_object_api=self.custom_objects,
            k8s_client=self.k8s_client,
            namespace=ns,
            _web_socket_stream_callable=self._get_websocket_mock(),
        )

    def _get_websocket_mock(self) -> Callable:
        if self._websocket_stream_exec_mock is not None:
            return self._websocket_stream_exec_mock
        else:

            def no_mock(*args: str, **kwargs: str) -> None:
                raise AssertionError(
                    "Websocket stream must be mocked during unit tests, please use with_mock_exec"
                )

            return no_mock

    def with_mocked_exec(self, success: bool, stdout: Optional[str]) -> Mock:
        exec_stream = self._mocker.MagicMock()
        # does not appear to effectively mock this function... we need to return the mock and pass it explicitly to
        # our KubeCtl#exec function...
        mock = self._mocker.patch("kubernetes.stream.stream", return_value=exec_stream)
        self._websocket_stream_exec_mock = mock
        if success:
            exec_stream.read_stdout.return_value = stdout if stdout is not None else ""
            exec_stream.read_channel.return_value = json.dumps(
                {"metadata": {}, "status": "Success"}
            )
        else:
            exec_stream.read_channel.return_value = json.dumps(
                {
                    "metadata": {},
                    "status": "Failure",
                    "message": "error message",
                    "reason": "InternalError",
                    "details": {"causes": [{"message": "error message"}]},
                    "code": 500,
                }
            )
        return mock


@dataclass
class ReleaseMock:
    kube_client_mock: KubeClientMock
    kube_client_deploy_mock: KubeClientMock

    def build_release(self, ns: str, cluster: str, release: str) -> Release:
        return Release(
            namespace=ns,
            cluster=cluster,
            name=release,
            _kubectl=self.kube_client_mock.build_client(ns),
            _kubectl_deploy=self.kube_client_deploy_mock.build_client(ns),
        )


@pytest.fixture
def mock_k8s_client(mocker: MockerFixture) -> KubeClientMock:
    return _mock_k8s_client(mocker)


@pytest.fixture
def mock_k8s_client_factory(mocker: MockerFixture) -> Callable[[], KubeClientMock]:
    return lambda: _mock_k8s_client(mocker)


def _mock_k8s_client(mocker: MockerFixture) -> KubeClientMock:
    k8s_client = mocker.Mock(ApiClient)
    config = namedtuple("config", ["host"])

    k8s_client.configuration = config("test_host")
    return KubeClientMock(
        core_v1=mocker.Mock(CoreV1Api),
        custom_objects=mocker.Mock(CustomObjectsApi),
        k8s_client=k8s_client,
        _mocker=mocker,
    )


@pytest.fixture
def mock_k8s_watch(mocker: MockerFixture) -> Mock:
    watch_mock: Mock = mocker.Mock(Watch)
    mocker.patch("cirrus_reindexer.k8s.Watch", new=lambda: watch_mock)
    return watch_mock


@pytest.fixture()
def mock_release(mocker: MockerFixture) -> ReleaseMock:
    return ReleaseMock(
        kube_client_mock=_mock_k8s_client(mocker),
        kube_client_deploy_mock=_mock_k8s_client(mocker),
    )


@pytest.fixture
def mock_subprocess_run(mocker: MockerFixture) -> Callable[[str, int], Mock]:
    def fn(result: str, returncode: Optional[int] = 0) -> Mock:
        mock_result = mocker.MagicMock()
        mock_result.returncode = returncode
        mock_result.stdout = result.encode("utf8")
        mock = mocker.patch("subprocess.run")
        mock.return_value = mock_result
        return mock

    return fn
