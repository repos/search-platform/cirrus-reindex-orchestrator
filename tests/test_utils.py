from typing import Callable

import pytest
from pytest_mock import MockerFixture

from cirrus_reindexer import utils


def test_expanddblist_ignores_blank_lines(
    mock_subprocess_run: Callable[[str], None],
) -> None:
    mock_subprocess_run("a\nb\n\n")
    assert utils.expanddblist("pytest") == {"a", "b"}


def test_expand_with_expanddblist(mocker: MockerFixture) -> None:
    dblists = {
        "all": set("abcd"),
        "x": set("ab"),
        "y": set("cd"),
        "z": set("ef"),
    }
    mocker.patch.object(utils, "expanddblist").side_effect = dblists.get

    assert utils.expand_with_expanddblist(["a", "b"]) == set("ab")
    assert utils.expand_with_expanddblist(["@all"]) == set("abcd")
    assert utils.expand_with_expanddblist(["@x", "@y"]) == set("abcd")
    assert utils.expand_with_expanddblist(["@x"]) == set("ab")
    assert utils.expand_with_expanddblist(["@y"]) == set("cd")
    assert utils.expand_with_expanddblist(["@all"], ["@x"]) == set("cd")
    assert utils.expand_with_expanddblist(["@y"], ["@x"]) == set("cd")
    assert utils.expand_with_expanddblist(["@all"], ["a", "b"]) == set("cd")

    assert utils.expand_with_expanddblist(["q"], verify=False) == set("q")
    with pytest.raises(ValueError):
        utils.expand_with_expanddblist(["q"])
