from concurrent.futures import ThreadPoolExecutor

import pytest

from cirrus_reindexer.work_queue import Task, WorkQueue


class MockTask(Task):
    _weight: int
    value: str
    called: bool

    def __init__(self, weight: int, value: str):
        self._weight = weight
        self.value = value
        self.called = False

    def __call__(self) -> str:
        self.called = True
        return self.value

    @property
    def weight(self):
        return self._weight


@pytest.fixture
def queue() -> WorkQueue[MockTask]:
    executor = ThreadPoolExecutor(max_workers=1)
    queue: WorkQueue[MockTask] = WorkQueue(
        executor, max_capacity=10, max_parallelism=10
    )
    return queue


def test_basic_task_execution(queue: WorkQueue[MockTask]) -> None:
    task = MockTask(1, "pytest")
    queue.add(task)
    results = list(queue.run())
    assert task.called
    assert results == [(task, task.value)]
    assert queue.shutdown() == []


def test_immediate_shutdown(queue: WorkQueue[MockTask]) -> None:
    task = MockTask(1, "do-not-run")
    queue.add(task)
    assert queue.shutdown() == []
    assert task.called is False


def test_shutdown_before_run(queue: WorkQueue[MockTask]) -> None:
    task = MockTask(1, "pytest")
    queue.add(task)
    # Shutdown only finishes in-flight futures. Anything
    # not already run is thrown away.
    assert list(queue.shutdown()) == []
    assert task.called is False


def test_highest_weight_first(queue: WorkQueue[MockTask]) -> None:
    queue.add(MockTask(1, "low"))
    queue.add(MockTask(5, "mid"))
    queue.add(MockTask(10, "high"))

    result = [value for task, value in queue.run()]
    assert result == ["high", "mid", "low"]


def test_runs_overweighted_tasks(queue: WorkQueue[MockTask]) -> None:
    queue.add(MockTask(100, "oversize"))
    result = [value for task, value in queue.run()]
    assert result == ["oversize"]


def test_sequencing(queue: WorkQueue[MockTask]) -> None:
    queue.add(MockTask(100, "oversize"))
    for x in range(2):
        queue.add(MockTask(9, "large"))
    for x in range(10):
        queue.add(MockTask(1, "small"))

    tasks = list(queue.queue)
    assert len(tasks) == 1 + 2 + 10

    gen = queue.run()
    # First iteration should run the oversized task
    task, result = next(gen)
    assert task.called
    assert result == "oversize"
    # None of the other tasks should have been started
    tasks.remove(task)
    assert all(task.called is False for task in tasks)

    # With oversized complete, next iteration should
    # start a large and a small task
    task, result = next(gen)
    assert sum(task.called for task in tasks if task.value == "large") == 1
    assert sum(task.called for task in tasks if task.value == "small") == 1
