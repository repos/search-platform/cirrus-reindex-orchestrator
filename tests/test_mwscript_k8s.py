import json
from collections.abc import Mapping
from io import StringIO
from typing import Callable, MutableMapping
from unittest.mock import Mock

import pytest
from kubernetes.client import (
    V1ContainerState,
    V1ContainerStateTerminated,
    V1ContainerStatus,
    V1ObjectMeta,
    V1Pod,
    V1PodList,
    V1PodStatus,
)
from pytest_mock import MockerFixture

from cirrus_reindexer.k8s import KubeCtl
from cirrus_reindexer.mwscript_k8s import (
    MwScript,
    MwScriptException,
    MwScriptExecution,
    MwScriptExecutionResult,
)
from conftest import KubeClientMock


def test_mwscript_k8s_start_success(
    mock_subprocess_run: Callable[[str], Mock],
    mock_k8s_client_factory: Callable[[], KubeClientMock],
) -> None:
    per_conf_kubectl_mock: Mapping[str, KubeClientMock] = {
        "kube_conf": mock_k8s_client_factory(),
        "kube_conf_deploy": mock_k8s_client_factory(),
    }
    per_conf_kubectl: MutableMapping[str, KubeCtl] = {}

    def kubectl_factory(conf: str, ns: str) -> KubeCtl:
        try:
            return per_conf_kubectl[conf]
        except KeyError:
            per_conf_kubectl[conf] = per_conf_kubectl_mock[conf].build_client(ns)
            return per_conf_kubectl[conf]

    mwscript: MwScript = MwScript(
        runner="mwscript-k8s", kubectl_factory=kubectl_factory
    )
    process_mock = mock_subprocess_run(
        json.dumps(
            {
                "mwscript": {
                    "cluster": "my_dc",
                    "config": "kube_conf",
                    "deploy_config": "kube_conf_deploy",
                    "job": "my-job-id",
                    "mediawiki_container": "my-container",
                    "namespace": "mw-script",
                    "release": "a1b2c3d4",
                },
                "error": None,
            }
        )
    )
    mw_exec = mwscript.start("my_script", "my_wiki", "my comment")
    assert process_mock.call_args.args == (
        [
            "mwscript-k8s",
            "--output",
            "json",
            "--comment",
            "my comment",
            "--",
            "my_script",
            "--wiki",
            "my_wiki",
        ],
    )
    assert mw_exec.job == "my-job-id"
    assert mw_exec.container == "my-container"
    assert mw_exec.release == "a1b2c3d4"
    assert mw_exec.cluster == "my_dc"
    assert mw_exec.kubectl == per_conf_kubectl["kube_conf"]
    assert mw_exec.kubectl_deploy == per_conf_kubectl["kube_conf_deploy"]
    assert mw_exec.kubectl.namespace == "mw-script"
    assert mw_exec.kubectl_deploy.namespace == "mw-script"


def test_mwscript_k8s_start_failure(
    mock_subprocess_run: Callable[[str], Mock],
    mock_k8s_client_factory: Callable[[], KubeClientMock],
) -> None:
    def kubectl_factory(conf: str, ns: str) -> KubeCtl:
        raise AssertionError("should not be called")

    mwscript: MwScript = MwScript(
        runner="mwscript-k8s", kubectl_factory=kubectl_factory
    )
    mock_subprocess_run(
        json.dumps({"mwscript": None, "error": "something wrong happened"})
    )
    with pytest.raises(MwScriptException):
        mwscript.start("my_script", "my_wiki", "my comment")


def test_mwscript_exec_result_cleanup(
    mock_subprocess_run: Callable[[str, int], Mock],
) -> None:
    exec_result = MwScriptExecutionResult(
        "my_ns",
        "my_job",
        "my_pod",
        "my_container",
        "my_release",
        "my_dc",
        0,
        "someoutput",
    )
    subprocess_mock = mock_subprocess_run("some output", 0)
    exec_result.cleanup()
    subprocess_mock.assert_called_once_with(
        [
            "helmfile",
            "--file",
            "/srv/deployment-charts/helmfile.d/services/my_ns/helmfile.yaml",
            "--environment",
            "my_dc",
            "--selector",
            "name=my_release",
            "destroy",
        ],
        env={"PATH": "/usr/bin", "RELEASE_NAME": "my_release"},
        check=True,
    )


def test_mwscript_exec_follow_happypath(
    mock_k8s_client: KubeClientMock,
    mock_k8s_watch: Mock,
    mock_subprocess_run: Callable[[str], Mock],
    mocker: MockerFixture,
) -> None:
    exec = MwScriptExecution(
        mock_k8s_client.build_client("my_ns"),
        mocker.Mock(KubeCtl),
        "my-job",
        "my-container",
        "a1b2c3d4",
        "my_dc",
    )
    tailed_logs = StringIO()
    # mock a terminated pod directly so that it matches the exit condition for both wait for started and wait for
    # completed
    container_status = V1ContainerStatus(
        image="img",
        image_id="img_id",
        name="my-container",
        ready=False,
        restart_count=0,
        state=V1ContainerState(terminated=V1ContainerStateTerminated(exit_code=0)),
    )
    v1pod = V1Pod(
        metadata=V1ObjectMeta(name="my-pod"),
        status=V1PodStatus(phase="Succeeded", container_statuses=[container_status]),
    )
    mock_k8s_client.core_v1.list_namespaced_pod.return_value = V1PodList(
        metadata=V1ObjectMeta(resource_version=1), items=[v1pod]
    )
    # mock the read log API (capture_stdout=True)
    mock_k8s_client.core_v1.read_namespaced_pod_log.return_value = (
        "log line 1\nlog line 2"
    )
    # mocl the stream API used by the tail log (stdout=tailed_logs)
    mock_k8s_watch.stream.return_value = ["log line 1", "log line 2"]

    result = exec.follow(stdout=tailed_logs, capture_stdout=True)

    assert result.pod == "my-pod"
    assert result.job == "my-job"
    assert result.container == "my-container"
    assert result.release == "a1b2c3d4"
    assert result.cluster == "my_dc"
    assert result.exit_status == 0
    assert result.stdout == "log line 1\nlog line 2"

    assert tailed_logs.getvalue() == "log line 1\nlog line 2\n"

    list_namespaced_pod_kwargs = (
        mock_k8s_client.core_v1.list_namespaced_pod.call_args.kwargs
    )
    assert list_namespaced_pod_kwargs["namespace"] == "my_ns"
    assert list_namespaced_pod_kwargs["label_selector"] == "job-name=my-job"
    read_namespaced_pod_log_kwargs = (
        mock_k8s_client.core_v1.read_namespaced_pod_log.call_args.kwargs
    )
    assert read_namespaced_pod_log_kwargs["namespace"] == "my_ns"
    assert read_namespaced_pod_log_kwargs["name"] == "my-pod"
    assert read_namespaced_pod_log_kwargs["container"] == "my-container"
