import json
from collections.abc import Callable
from io import StringIO
from typing import Mapping
from unittest.mock import Mock

import pytest
import requests_mock
from cirrus_toolbox.mw_api_client import MWClient
from pytest_mock import MockerFixture

from cirrus_reindexer.cirrus import (
    CirrusConnectionInfo,
    CirrusIndexAlias,
    ReindexResult,
    get_live_indices,
    running_reindexes,
)
from cirrus_reindexer.mwscript_k8s import MwScript, MwScriptExecutionResult


def test_live_indices_happy_path() -> None:
    base_url = "https://search-alpha.dc1:4444"
    alias = "pytest_content"
    index = f"{alias}_12345"
    with requests_mock.Mocker() as m:
        m.get(
            f"{base_url}/_cat/aliases/{alias}",
            text=json.dumps(
                [
                    {"alias": alias, "index": index},
                ]
            ),
        )
        result = get_live_indices(base_url, alias)
    assert result == [index]


CLUSTER_ENTRY_PUBLIC: Mapping = {
    "group": "alpha",
    "aliases": [
        "pytestwiki_content",
        "pytestwiki_general",
        "pytestwiki_ignored",
    ],
    "shard_count": {
        "pytestwiki_content": 1,
        "pytestwiki_general": 2,
        "pytestwiki_ignored": 3,
    },
}

CLUSTER_ENTRY: Mapping = {
    **CLUSTER_ENTRY_PUBLIC,
    "connection": [
        {
            "host": "search-alpha.dc1",
            "transport": "Https",
            "port": 3333,
        }
    ],
}


def test_base_url_from_config() -> None:
    base_url = CirrusConnectionInfo._wiki_base_url(CLUSTER_ENTRY)
    assert base_url == "https://search-alpha.dc1:3333"


def test_init_from_external_state(
    mocker: MockerFixture, mock_subprocess_run: Callable[[str, int], Mock]
) -> None:
    dbname = "pytestwiki"
    # mock the mwscript-k8s run used to capture the connection info
    mwscript_k8s = mocker.Mock(MwScript)
    mwscript_k8s_exec_result = MwScriptExecutionResult(
        "my_ns",
        "my_job",
        "my_pod",
        "my_container",
        "my_release",
        "my_cluster",
        0,
        json.dumps(
            {
                "dbname": dbname,
                "clusters": {
                    "dc1": CLUSTER_ENTRY,
                },
            }
        ),
    )
    mock_subprocess_mwscript_cleanup = mock_subprocess_run("destroyed", 0)
    mwscript_k8s.run.return_value = mwscript_k8s_exec_result
    mwclient = mocker.Mock(MWClient)
    mwclient.expected_indices.return_value = {
        "dbname": dbname,
        "clusters": {
            "dc1": CLUSTER_ENTRY_PUBLIC,
        },
    }
    conn_info = CirrusConnectionInfo(mwscript_runner=mwscript_k8s)
    index_aliases = list(
        CirrusIndexAlias.from_external_state(dbname, "dc1", mwclient, conn_info)
    )

    assert len(index_aliases) == 2
    assert {cirrus_index.alias_name for cirrus_index in index_aliases} == {
        "pytestwiki_content",
        "pytestwiki_general",
    }
    for cirrus_index in index_aliases:
        assert cirrus_index.dbname == dbname
        assert cirrus_index.replica == "dc1"
        assert cirrus_index.base_url == "https://search-alpha.dc1:3333"
        assert (
            cirrus_index.shard_count
            == CLUSTER_ENTRY_PUBLIC["shard_count"][cirrus_index.alias_name]
        )

    # Call a second time to ensure that we do not call mwscript-k8s multiple times to fetch the connection info
    CirrusIndexAlias.from_external_state(dbname, "dc1", mwclient, conn_info)
    mwscript_k8s.run.assert_called_once_with(
        "extensions/CirrusSearch/maintenance/ExpectedIndices.php",
        dbname,
        [],
        comment="cirrus-reindex-orchestrator",
        check=True,
        capture_stdout=True,
    )
    mock_subprocess_mwscript_cleanup.assert_called_once_with(
        [
            "helmfile",
            "--file",
            "/srv/deployment-charts/helmfile.d/services/my_ns/helmfile.yaml",
            "--environment",
            "my_cluster",
            "--selector",
            "name=my_release",
            "destroy",
        ],
        env={"PATH": "/usr/bin", "RELEASE_NAME": "my_release"},
        check=True,
    )


def test_running_reindexes():
    base_url = "https://search-alpha.dc1:4444"
    alias = "pytestwiki_content"
    task = {
        "action": "indices:data/write/reindex",
        "description": f"Reindex of [{alias}] to [{alias}_12345][_doc]",
    }
    with requests_mock.Mocker() as m:
        m.get(
            f"{base_url}/_tasks?detailed=true",
            text=json.dumps({"nodes": {"a": {"tasks": {"a:1": task}}}}),
        )
        result = running_reindexes(base_url)
    assert result == {"a:1": alias}


testdata_reindex_one_wiki_index = [
    [0, ReindexResult.SUCCESS],
    [10, ReindexResult.UNNECESSARY],
    [1, ReindexResult.ERROR],
]


@pytest.mark.parametrize(
    "exist_status,expected_result", testdata_reindex_one_wiki_index
)
def test_reindex_one_wiki_index(
    exist_status: int,
    expected_result: ReindexResult,
    mocker: MockerFixture,
    mock_subprocess_run: Callable[[str], Mock],
) -> None:
    runner = mocker.Mock(MwScript)
    runner.run.return_value = MwScriptExecutionResult(
        "my_ns",
        "my_job",
        "my_pod",
        "my_container",
        "my_release",
        "my_cluster",
        exist_status,
        None,
    )
    mwscript_cleanup_mock = mock_subprocess_run("mwscript cleanup")

    cirrus_index_alias = CirrusIndexAlias(
        "pytest_general",
        "pytest",
        "dc1",
        "https://search.svc.dc1.unittest.local",
        2,
        runner,
    )
    tail_log_to = StringIO()
    result = cirrus_index_alias.reindex(False, tail_log_to)
    assert result is expected_result

    runner.run.assert_called_once_with(
        "extensions/CirrusSearch/maintenance/UpdateOneSearchIndexConfig.php",
        "pytest",
        [
            "--cluster=dc1",
            "--reindexAndRemoveOk",
            "--indexIdentifier=now",
            "--indexSuffix=general",
        ],
        comment="cirrus-reindex-orchestrator",
        capture_stdout=False,
        stdout=tail_log_to,
        check=False,
    )
    mwscript_cleanup_mock.assert_called_once_with(
        [
            "helmfile",
            "--file",
            "/srv/deployment-charts/helmfile.d/services/my_ns/helmfile.yaml",
            "--environment",
            "my_cluster",
            "--selector",
            "name=my_release",
            "destroy",
        ],
        env={"PATH": "/usr/bin", "RELEASE_NAME": "my_release"},
        check=True,
    )
