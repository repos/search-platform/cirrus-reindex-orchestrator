from io import StringIO
from unittest.mock import Mock

import pytest
from kubernetes.client import (
    V1ConfigMap,
    V1ConfigMapList,
    V1ContainerImage,
    V1ContainerState,
    V1ContainerStateRunning,
    V1ContainerStateTerminated,
    V1ContainerStatus,
    V1ObjectMeta,
    V1Pod,
    V1PodList,
    V1PodStatus,
)

from cirrus_reindexer.k8s import KubeCtl, KubectlException
from conftest import KubeClientMock


def test_find_pod_name_no_items(mock_k8s_client: KubeClientMock) -> None:
    mock_k8s_client.core_v1.list_namespaced_pod.return_value = V1PodList(items=[])
    with pytest.raises(KubectlException):
        mock_k8s_client.build_client("my_ns").find_pod_name(
            labels={"mylabel": "myvalue"}
        )
    mock_k8s_client.core_v1.list_namespaced_pod.assert_called_with(
        namespace="my_ns", label_selector="mylabel=myvalue"
    )


def test_find_pod_name_happy_path(mock_k8s_client: KubeClientMock) -> None:
    mock_k8s_client.core_v1.list_namespaced_pod.return_value = V1PodList(
        items=[V1Pod(metadata=V1ObjectMeta(name="pod_b"))]
    )
    pod = mock_k8s_client.build_client("my_ns").find_pod_name(
        labels={"mylabel": "myvalue"}
    )
    assert pod == "pod_b"
    mock_k8s_client.core_v1.list_namespaced_pod.assert_called_with(
        namespace="my_ns", label_selector="mylabel=myvalue"
    )


def test_list_pod(mock_k8s_client: KubeClientMock) -> None:
    expected_pod_list = V1PodList(
        items=[
            V1Pod(metadata=V1ObjectMeta(name="pod_a")),
            V1Pod(metadata=V1ObjectMeta(name="pod_b")),
        ]
    )
    mock_k8s_client.core_v1.list_namespaced_pod.return_value = expected_pod_list
    actual_pod_list = mock_k8s_client.build_client("my_ns").list_pods(
        labels={"mylabel": "myvalue"}
    )
    assert actual_pod_list == expected_pod_list
    mock_k8s_client.core_v1.list_namespaced_pod.assert_called_with(
        namespace="my_ns", label_selector="mylabel=myvalue"
    )


def test_wait_until_pod_container(
    mock_k8s_client: KubeClientMock, mock_k8s_watch: Mock
) -> None:
    def my_cond(pod: V1Pod, container: str) -> bool:
        assert container == "my_container"
        return bool(pod.metadata.name == "mine")

    mock_k8s_client.core_v1.list_namespaced_pod.return_value = V1PodList(
        metadata=V1ObjectMeta(resource_version=1), items=[]
    )

    expected_pod = V1Pod(metadata=V1ObjectMeta(name="mine"))
    mock_k8s_watch.stream.return_value = [
        {"object": V1Pod(metadata=V1ObjectMeta(name="not_mine"))},
        {"object": expected_pod},
    ]

    actual_pod = mock_k8s_client.build_client("my_ns").wait_until_pod_container(
        labels={"mylabel": "myvalue"}, container="my_container", test_func=my_cond
    )
    assert actual_pod == expected_pod


def test_wait_until_pod_container_timeout(
    mock_k8s_client: KubeClientMock, mock_k8s_watch: Mock
) -> None:
    def my_cond(pod: V1Pod, container: str) -> bool:
        assert container == "my_container"
        return bool(pod.metadata.name == "mine")

    mock_k8s_client.core_v1.list_namespaced_pod.return_value = V1PodList(
        metadata=V1ObjectMeta(resource_version=1), items=[]
    )

    mock_k8s_watch.stream.return_value = [
        {"object": V1Pod(metadata=V1ObjectMeta(name="not_mine"))},
    ]

    with pytest.raises(KubectlException):
        mock_k8s_client.build_client("my_ns").wait_until_pod_container(
            labels={"mylabel": "myvalue"}, container="my_container", test_func=my_cond
        )


def _pod_status(
    phase: str, container: str, container_state: V1ContainerState
) -> V1PodStatus:
    return V1PodStatus(
        phase=phase,
        container_statuses=[
            V1ContainerStatus(
                name=container,
                image=V1ContainerImage(names=[]),
                restart_count=0,
                ready=True,
                image_id="image",
                state=container_state,
            )
        ],
    )


testdata_is_pod_container_started = [
    (V1Pod(status=V1PodStatus(phase="Running")), "container", True, False),
    (V1Pod(status=V1PodStatus(phase="Succeeded")), "container", True, True),
    (V1Pod(status=V1PodStatus(phase="Failed")), "container", True, True),
    (V1Pod(status=V1PodStatus(phase="Unknown")), "container", False, False),
    (
        V1Pod(
            status=_pod_status(
                "Pending",
                "container",
                V1ContainerState(running=V1ContainerStateRunning()),
            )
        ),
        "container",
        True,
        False,
    ),
    (
        V1Pod(
            status=_pod_status(
                "Pending",
                "container",
                V1ContainerState(terminated=V1ContainerStateTerminated(exit_code=0)),
            )
        ),
        "container",
        True,
        True,
    ),
    (
        V1Pod(
            status=_pod_status(
                "Pending",
                "not_mine",
                V1ContainerState(running=V1ContainerStateRunning()),
            )
        ),
        "container",
        False,
        False,
    ),
    (
        V1Pod(
            status=_pod_status(
                "Pending",
                "not_mine",
                V1ContainerState(terminated=V1ContainerStateTerminated(exit_code=0)),
            )
        ),
        "container",
        False,
        False,
    ),
]


@pytest.mark.parametrize(
    "pod,container,started,stopped", testdata_is_pod_container_started
)
def test_is_pod_container_started_stopped(
    pod: V1Pod, container: str, started: bool, stopped: bool
) -> None:
    assert KubeCtl.is_pod_container_started(pod, container) == started
    assert KubeCtl.is_pod_container_stopped(pod, container) == stopped


def test_read_pod_logs(mock_k8s_client: KubeClientMock) -> None:
    mock_k8s_client.core_v1.read_namespaced_pod_log.return_value = "my logs"
    pod = "my_pod"
    container = "my_container"
    ns = "my_ns"

    logs = mock_k8s_client.build_client(ns).get_pod_logs(pod, container)
    assert logs == "my logs"
    assert mock_k8s_client.core_v1.read_namespaced_pod_log.call_args.kwargs == {
        "name": pod,
        "namespace": ns,
        "container": container,
    }


def test_tail_pod_logs(mock_k8s_client: KubeClientMock, mock_k8s_watch: Mock) -> None:
    lines = [f"line {i}" for i in range(1, 20)]
    mock_k8s_watch.stream.return_value = lines
    pod = "my_pod"
    container = "my_container"
    ns = "my_ns"

    io = StringIO()
    mock_k8s_client.build_client(ns).tail_pod_logs(pod, container, io)
    assert mock_k8s_watch.stream.call_args.kwargs == {
        "name": pod,
        "namespace": ns,
        "container": container,
    }
    assert io.getvalue() == "\n".join(lines) + "\n"


def test_find_configmap_by_suffix_happypath(mock_k8s_client: KubeClientMock) -> None:
    ours = V1ConfigMap(
        metadata=V1ObjectMeta(name="flink-app-consumer-flink-app-config")
    )
    other = V1ConfigMap(
        metadata=V1ObjectMeta(name="flink-app-consumer-envoy-config-volume")
    )
    mock_k8s_client.core_v1.list_namespaced_config_map.return_value = V1ConfigMapList(
        items=[ours, other]
    )
    result = mock_k8s_client.build_client("my_ns").find_configmap_by_suffix(
        "flink-app-config", labels={"mylabel": "myvalue"}
    )
    assert result == ours
    mock_k8s_client.core_v1.list_namespaced_config_map.assert_called_with(
        namespace="my_ns", label_selector="mylabel=myvalue"
    )


def test_find_configmap_not_returned(mock_k8s_client: KubeClientMock) -> None:
    ours = V1ConfigMap(
        metadata=V1ObjectMeta(name="flink-app-consumer-flink-app-config")
    )
    other = V1ConfigMap(
        metadata=V1ObjectMeta(name="flink-app-consumer-envoy-config-volume")
    )
    mock_k8s_client.core_v1.list_namespaced_config_map.return_value = V1ConfigMapList(
        items=[ours, other]
    )
    with pytest.raises(KubectlException):
        mock_k8s_client.build_client("my_ns").find_configmap_by_suffix(
            "unrelated", labels={"mylabel": "myvalue"}
        )
    mock_k8s_client.core_v1.list_namespaced_config_map.assert_called_with(
        namespace="my_ns", label_selector="mylabel=myvalue"
    )


def test_find_configmap_nothing_found(mock_k8s_client: KubeClientMock) -> None:
    mock_k8s_client.core_v1.list_namespaced_config_map.return_value = V1ConfigMapList(
        items=[]
    )
    with pytest.raises(KubectlException):
        mock_k8s_client.build_client("my_ns").find_configmap_by_suffix(
            "unrelated", labels={"mylabel": "myvalue"}
        )
    mock_k8s_client.core_v1.list_namespaced_config_map.assert_called_with(
        namespace="my_ns", label_selector="mylabel=myvalue"
    )


def test_flink_status_not_deployed(mock_k8s_client: KubeClientMock) -> None:
    mock_k8s_client.custom_objects.list_namespaced_custom_object.return_value = {
        "items": [],
    }
    status = mock_k8s_client.build_client("my_ns").get_flink_deployment_status(
        labels={"mylabel": "myvalue"}
    )
    assert status == "NOT_DEPLOYED"
    mock_k8s_client.custom_objects.list_namespaced_custom_object.assert_called_with(
        group="flink.apache.org",
        version="v1beta1",
        plural="flinkdeployments",
        namespace="my_ns",
        label_selector="mylabel=myvalue",
    )


def test_flink_status_no_state(mock_k8s_client: KubeClientMock) -> None:
    mock_k8s_client.custom_objects.list_namespaced_custom_object.return_value = {
        "items": [
            {
                "status": {"jobStatus": {}},
            }
        ],
    }
    status = mock_k8s_client.build_client("my_ns").get_flink_deployment_status(
        labels={"mylabel": "myvalue"}
    )
    assert status == "UNKNOWN"
    mock_k8s_client.custom_objects.list_namespaced_custom_object.assert_called_with(
        group="flink.apache.org",
        version="v1beta1",
        plural="flinkdeployments",
        namespace="my_ns",
        label_selector="mylabel=myvalue",
    )


def test_flink_status_running(mock_k8s_client: KubeClientMock) -> None:
    mock_k8s_client.custom_objects.list_namespaced_custom_object.return_value = {
        "items": [
            {
                "status": {"jobStatus": {"state": "RUNNING"}},
            }
        ],
    }
    status = mock_k8s_client.build_client("my_ns").get_flink_deployment_status(
        labels={"mylabel": "myvalue"}
    )
    assert status == "RUNNING"
    mock_k8s_client.custom_objects.list_namespaced_custom_object.assert_called_with(
        group="flink.apache.org",
        version="v1beta1",
        plural="flinkdeployments",
        namespace="my_ns",
        label_selector="mylabel=myvalue",
    )


def test_exec_happy_path(mock_k8s_client: KubeClientMock) -> None:
    expected_output = "my success output"
    exec_mock = mock_k8s_client.with_mocked_exec(
        success=True, stdout="my success output"
    )
    actual_output = mock_k8s_client.build_client("test_ns").exec(
        "my_pod", "my_container", "ls"
    )
    assert actual_output == expected_output
    assert exec_mock.call_args.args[1] == "my_pod"
    assert exec_mock.call_args.kwargs["namespace"] == "test_ns"
    assert exec_mock.call_args.kwargs["container"] == "my_container"
    assert exec_mock.call_args.kwargs["command"] == ("ls",)


def test_exec_error(mock_k8s_client: KubeClientMock) -> None:
    exec_mock = mock_k8s_client.with_mocked_exec(success=False, stdout="success")
    with pytest.raises(KubectlException):
        mock_k8s_client.build_client("test_ns").exec("my_pod", "my_container", "ls")
    assert exec_mock.call_args.args[1] == "my_pod"
    assert exec_mock.call_args.kwargs["namespace"] == "test_ns"
    assert exec_mock.call_args.kwargs["container"] == "my_container"
    assert exec_mock.call_args.kwargs["command"] == ("ls",)
