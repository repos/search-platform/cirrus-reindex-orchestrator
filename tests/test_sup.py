import time
from datetime import datetime
from itertools import count
from pathlib import Path
from threading import Condition

import pytest
import yaml
from kubernetes.client import V1ConfigMap, V1ConfigMapList, V1ObjectMeta
from pytest_mock import MockerFixture

from cirrus_reindexer import sup
from cirrus_reindexer.cirrus import CirrusIndexAlias
from cirrus_reindexer.k8s import KubectlException, Release
from cirrus_reindexer.state import ReindexingState
from conftest import ReleaseMock


@pytest.mark.parametrize(
    "replica,expect",
    [
        (
            "eqiad",
            Release("cirrus-streaming-updater", "eqiad", "consumer-search-backfill"),
        ),
        (
            "codfw",
            Release("cirrus-streaming-updater", "codfw", "consumer-search-backfill"),
        ),
        (
            "cloudelastic",
            Release(
                "cirrus-streaming-updater", "eqiad", "consumer-cloudelastic-backfill"
            ),
        ),
    ],
)
def test_releases(replica: str, expect: Release) -> None:
    release = sup.backfill_release_for_cluster(replica)
    assert expect == release


def test_detect_backfill_request(mock_release: ReleaseMock) -> None:
    start_at = datetime(2024, 1, 1)
    end_at = datetime(2024, 1, 1, 1)
    wikiids = {"a", "b", "c"}
    index_aliases = {"x", "y", "z"}
    mock_release.kube_client_mock.core_v1.list_namespaced_config_map.return_value = (
        V1ConfigMapList(
            items=[
                V1ConfigMap(
                    metadata=V1ObjectMeta(name="flink-app-config"),
                    data={
                        "app.config.yaml": yaml.dump(
                            {
                                sup.START_TIME: start_at.isoformat() + "Z",
                                sup.END_TIME: end_at.isoformat() + "Z",
                                sup.WIKIIDS: ";".join(wikiids),
                                sup.INDEX_ALIASES: ";".join(index_aliases),
                            }
                        ),
                    },
                )
            ]
        )
    )
    release = mock_release.build_release("my_ns", "pytest", "my-release")

    found = sup.detect_backfill_request(release)

    assert found.index_aliases == index_aliases
    assert found.start_at == start_at
    assert found.end_at == end_at


@pytest.fixture
def backfiller(tmp_path: Path, mocker: MockerFixture) -> sup.Backfiller:
    backfiller = sup.Backfiller(
        ReindexingState(tmp_path / "state.json"),
        Release("ns", "release", "name"),
        (tmp_path / "backfill.log" for _ in count()),
    )

    # Signals no in-progress backfill
    mocker.patch.object(sup, "detect_backfill_request").side_effect = KubectlException()

    return backfiller


@pytest.mark.timeout(2)
def test_backfiller_stops_on_empty(backfiller: sup.Backfiller) -> None:
    # Should be simple start and stop with nothing to do
    backfiller.start()
    backfiller.shutdown()
    backfiller.join()


class TestFinalBackfill:
    def setup(self, backfiller: sup.Backfiller, mocker: MockerFixture) -> None:
        state = backfiller.state

        # When the backfiller attempts to run a backfill wait around until
        # the condition is notified.
        self.complete_backfill_cond = Condition()

        def wait_for_condition(*args):
            with self.complete_backfill_cond:
                self.complete_backfill_cond.wait()
            return True

        mocker.patch.object(sup, "backfill").side_effect = wait_for_condition

        # populate the state with something to work with
        self.alias_a = CirrusIndexAlias("a", "dbname", "replica", "base_url", 1)
        self.a_name = mocker.patch.object(self.alias_a, "live_index_name")
        self.a_name.return_value = "a_1"
        state.add_reindex_target(self.alias_a)
        state.reindex_started(self.alias_a)

        self.alias_b = CirrusIndexAlias("b", "dbname", "replica", "base_url", 1)
        self.b_name = mocker.patch.object(self.alias_b, "live_index_name")
        self.b_name.return_value = "b_1"
        state.add_reindex_target(self.alias_b)
        state.reindex_started(self.alias_b)

    def wait_for_backfill_start(self) -> None:
        # We have to sleep a moment to ensure the other thread runs
        while not len(self.complete_backfill_cond._waiters):  # type: ignore
            time.sleep(0.01)

    def complete_backfill(self) -> None:
        self.wait_for_backfill_start()
        with self.complete_backfill_cond:
            self.complete_backfill_cond.notify()

    def assert_shutdown(self, backfiller: sup.Backfiller) -> None:
        # No new backfills, it should shutdown now
        for i in range(10):
            if not backfiller.is_alive():
                break
            time.sleep(0.1)
        else:
            # Last chance, but it looks like backfiller failed to shutdown
            assert not backfiller.is_alive()

        # All backfills must be complete
        assert not backfiller.state.pending_backfill()

    def test_backfiller_runs_final_backfill(
        self, backfiller: sup.Backfiller, mocker: MockerFixture
    ) -> None:
        """
        In this test case the final backfill is added while
        the previous backfill is running
        """
        self.setup(backfiller, mocker)
        state = backfiller.state

        # Add the first alias as a pending backfill
        self.a_name.return_value = "a_2"
        state.reindex_completed(self.alias_a, False)
        assert state.pending_backfill()
        backfiller.start()
        self.wait_for_backfill_start()

        # Now that a backfill is running, add the second backfill that is
        # expected to run.
        self.b_name.return_value = "b_2"
        state.reindex_completed(self.alias_b, False)

        # Tell the backfiller that no new backfills will be registered
        backfiller.shutdown()
        # Complete the in-progress backfill
        self.complete_backfill()
        # A new backfill should have started, complete that too
        self.complete_backfill()
        # Once the second backfill is complete it should shutdown
        self.assert_shutdown(backfiller)

    def test_backfiller_runs_final_backfill_v2(
        self, backfiller: sup.Backfiller, mocker: MockerFixture
    ) -> None:
        """
        In this test case the final backfill is added while
        the backfiller is waiting on the shutdown signal.
        """
        self.setup(backfiller, mocker)
        state = backfiller.state

        # Add the first alias as a pending backfill
        self.a_name.return_value = "a_2"
        state.reindex_completed(self.alias_a, False)
        assert state.pending_backfill()
        backfiller.start()

        # Complete the in-progress backfill
        self.complete_backfill()

        # wait for the backfiller to be waiting on the shutdown conditional
        while not len(backfiller.shutdown_requested._cond._waiters):  # type: ignore
            time.sleep(0.01)

        # The requested backfill should have been completed, nothing
        # should be pending
        assert not state.pending_backfill()

        # Add the second backfill that is expected to run.
        self.b_name.return_value = "b_2"
        state.reindex_completed(self.alias_b, False)
        assert state.pending_backfill()

        # Tell the backfiller that no new backfills will be registered
        backfiller.shutdown()

        # Finish the next backfill
        self.complete_backfill()
        # Once the second backfill is complete it should shutdown
        self.assert_shutdown(backfiller)
