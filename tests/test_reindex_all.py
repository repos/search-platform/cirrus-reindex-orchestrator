import inspect
from pathlib import Path

from pytest_mock import MockerFixture

from cirrus_reindexer import reindex_all
from cirrus_reindexer.reindex_all import arg_parser, main, post_process_args


def test_post_process_args_happy_path(mocker: MockerFixture, tmp_path: Path) -> None:
    mocker.patch.object(reindex_all, "expand_with_expanddblist").return_value = {
        "pytestwiki"
    }
    source_kwargs = dict(
        vars(
            arg_parser().parse_args(
                [
                    "--cirrus-cluster=eqiad",
                    f"--log-dir={tmp_path}",
                    "--dbnames=pytestwiki",
                ]
            )
        )
    )
    kwargs = post_process_args(source_kwargs)
    parameters = inspect.signature(main).parameters
    assert set(kwargs.keys()) == set(parameters.keys())
    assert kwargs["dbnames"] == {"pytestwiki"}
    assert kwargs["log_dir"] == tmp_path
