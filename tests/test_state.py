from pathlib import Path

import pytest
from pytest_mock import MockerFixture

from cirrus_reindexer.cirrus import CirrusIndexAlias
from cirrus_reindexer.state import ReindexingState


@pytest.fixture
def cirrus_index() -> CirrusIndexAlias:
    return CirrusIndexAlias(
        "dbname_content", "dbname", "replica", "https://pytest.elastic:9200", 1
    )


def test_empty_state(tmp_path: Path, cirrus_index: CirrusIndexAlias) -> None:
    state = ReindexingState(tmp_path / "state.json")
    assert not state.pending_reindex()
    assert not state.pending_backfill()
    assert not state.is_reindexing_completed(cirrus_index)


def test_reload_missing_state(tmp_path: Path) -> None:
    state = ReindexingState(tmp_path / "state.json")
    with pytest.raises(FileNotFoundError):
        state.reload()


def test_reload_state(
    mocker: MockerFixture, tmp_path: Path, cirrus_index: CirrusIndexAlias
) -> None:
    state = ReindexingState(tmp_path / "state.json")
    mocker.patch.object(cirrus_index, "live_index_name").return_value = "a"
    state.add_reindex_target(cirrus_index)

    state = ReindexingState(tmp_path / "state.json")
    state.reload()
    assert cirrus_index.alias_name in state.aliases
    assert cirrus_index == state.aliases[cirrus_index.alias_name]


def test_backfill_only_when_changing(
    mocker: MockerFixture, tmp_path: Path, cirrus_index: CirrusIndexAlias
) -> None:
    now = 0.0
    state = ReindexingState(tmp_path / "state.json", clock=lambda: now)

    mocker.patch.object(cirrus_index, "live_index_name").return_value = "a"
    state.add_reindex_target(cirrus_index)
    cirrus_index = state.aliases[cirrus_index.alias_name]
    live_index_name = mocker.patch.object(cirrus_index, "live_index_name")
    live_index_name.return_value = "a"

    now += 1
    state.reindex_started(cirrus_index)
    now += 1
    state.reindex_completed(cirrus_index, False)

    assert not state.pending_backfill()

    now += 1
    backfill_from = now
    state.reindex_started(cirrus_index)
    live_index_name.return_value = "b"
    now += 1
    backfill_to = now
    state.reindex_completed(cirrus_index, False)

    request = state.pending_backfill()
    assert request.start_at.timestamp() == backfill_from
    assert request.end_at.timestamp() == backfill_to
    assert request.index_aliases == {cirrus_index.alias_name}


def test_can_complete_reindex_without_reindexing(
    mocker: MockerFixture, tmp_path: Path, cirrus_index: CirrusIndexAlias
) -> None:
    now = 0.0
    state = ReindexingState(tmp_path / "state.json", clock=lambda: now)
    mocker.patch.object(cirrus_index, "live_index_name").return_value = "a"
    state.add_reindex_target(cirrus_index)
    cirrus_index = state.aliases[cirrus_index.alias_name]
    live_index_name = mocker.patch.object(cirrus_index, "live_index_name")
    live_index_name.return_value = "a"

    now += 1
    state.reindex_started(cirrus_index)
    now += 1
    state.reindex_completed(cirrus_index, True)

    assert state.is_reindexing_completed(cirrus_index)
    assert not state.pending_reindex()
    assert not state.pending_backfill()


def test_sequencing(
    mocker: MockerFixture, tmp_path: Path, cirrus_index: CirrusIndexAlias
) -> None:
    now = 0.0
    state = ReindexingState(tmp_path / "state.json", clock=lambda: now)
    other_index = CirrusIndexAlias(
        "otherwiki_content", "otherwiki", cirrus_index.replica, cirrus_index.base_url, 1
    )

    mocker.patch.object(cirrus_index, "live_index_name").return_value = "a"
    mocker.patch.object(other_index, "live_index_name").return_value = "z"
    state.add_reindex_target(cirrus_index)
    state.add_reindex_target(other_index)
    # When the index was added it went through a serde, we need the internal one
    # to be able to change cirrus_index.live_index_name()
    cirrus_index = state.aliases[cirrus_index.alias_name]
    other_index = state.aliases[other_index.alias_name]
    mocker.patch.object(cirrus_index, "live_index_name").return_value = "a"
    mocker.patch.object(other_index, "live_index_name").return_value = "z"

    assert {x.alias_name for x in state.pending_reindex()} == {
        cirrus_index.alias_name,
        other_index.alias_name,
    }
    assert not state.pending_backfill()
    assert not state.is_reindexing_completed(cirrus_index)

    # Start running new state.
    now += 1
    for pending in state.pending_reindex():
        state.reindex_started(pending)

    # Starting the reindex operations shouldn't have changed anything.
    # In-flight reindexes are still considered pending. They are not
    # expected to be queried, except on startup.
    assert {x.alias_name for x in state.pending_reindex()} == {
        cirrus_index.alias_name,
        other_index.alias_name,
    }
    assert not state.pending_backfill()
    assert not state.is_reindexing_completed(cirrus_index)

    # Complete the reindex operation. Index has changed.
    now += 1
    mocker.patch.object(cirrus_index, "live_index_name").return_value = "b"
    state.reindex_completed(cirrus_index, False)

    assert state.pending_backfill().index_aliases == {cirrus_index.alias_name}
    assert state.pending_reindex() == [other_index]

    # Record the current backfill
    backfill = state.pending_backfill()

    # reindex the other index
    now += 1
    state.reindex_started(other_index)
    now += 1
    mocker.patch.object(other_index, "live_index_name").return_value = "y"
    state.reindex_completed(other_index, False)

    # All reindexes are now completed. A second backfill has registered.
    assert not state.pending_reindex()
    assert state.pending_backfill().index_aliases == {
        cirrus_index.alias_name,
        other_index.alias_name,
    }
    assert state.is_reindexing_completed(cirrus_index)
    assert state.is_reindexing_completed(other_index)

    # Finish the previously captured backfill
    now += 1
    state.backfill_completed(backfill)

    # One backfill should still be pending
    assert state.pending_backfill().index_aliases == {other_index.alias_name}

    # Complete final backfill
    now += 1
    state.backfill_completed(state.pending_backfill())

    # Everything is now complete
    assert not state.pending_reindex()
    assert not state.pending_backfill()
    assert state.is_reindexing_completed(cirrus_index)
    assert state.is_reindexing_completed(other_index)
