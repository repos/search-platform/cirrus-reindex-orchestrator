import json

import pytest
from kubernetes.client import V1ObjectMeta, V1Pod, V1PodList

from cirrus_reindexer.flink import KubectlException, flink_status_via_rest
from conftest import ReleaseMock


def test_flink_rest_no_pod_found(mock_release: ReleaseMock) -> None:
    mock_release.kube_client_mock.core_v1.list_namespaced_pod.return_value = V1PodList(
        items=[]
    )
    with pytest.raises(KubectlException):
        flink_status_via_rest(
            mock_release.build_release("my_ns", "cluster", "my_release")
        )
    mock_release.kube_client_mock.core_v1.list_namespaced_pod.assert_called_with(
        namespace="my_ns", label_selector="release=my_release,component=jobmanager"
    )


def test_flink_rest_failure(mock_release: ReleaseMock) -> None:
    mock_release.kube_client_mock.core_v1.list_namespaced_pod.return_value = V1PodList(
        items=[V1Pod(metadata=V1ObjectMeta(name="my_pod"))]
    )

    exec_mock = mock_release.kube_client_deploy_mock.with_mocked_exec(
        False,
        json.dumps(
            {
                "jobs": [{"status": "RUNNING"}],
            }
        ),
    )
    with pytest.raises(KubectlException):
        flink_status_via_rest(
            mock_release.build_release("my_ns", "cluster", "my_release")
        )

    mock_release.kube_client_mock.core_v1.list_namespaced_pod.assert_called_with(
        namespace="my_ns", label_selector="release=my_release,component=jobmanager"
    )
    assert exec_mock.call_args.args[1] == "my_pod"
    assert exec_mock.call_args.kwargs["namespace"] == "my_ns"
    assert exec_mock.call_args.kwargs["container"] == "flink-main-container"
    assert "python3" in exec_mock.call_args.kwargs["command"]


def test_flink_rest_no_jobs(mock_release: ReleaseMock) -> None:
    mock_release.kube_client_mock.core_v1.list_namespaced_pod.return_value = V1PodList(
        items=[V1Pod(metadata=V1ObjectMeta(name="my_pod"))]
    )

    exec_mock = mock_release.kube_client_deploy_mock.with_mocked_exec(
        True, json.dumps({})
    )
    with pytest.raises(KubectlException):
        flink_status_via_rest(
            mock_release.build_release("my_ns", "cluster", "my_release")
        )

    mock_release.kube_client_mock.core_v1.list_namespaced_pod.assert_called_with(
        namespace="my_ns", label_selector="release=my_release,component=jobmanager"
    )
    assert exec_mock.call_args.args[1] == "my_pod"
    assert exec_mock.call_args.kwargs["namespace"] == "my_ns"
    assert exec_mock.call_args.kwargs["container"] == "flink-main-container"
    assert "python3" in exec_mock.call_args.kwargs["command"]


def test_flink_rest_running(mock_release: ReleaseMock) -> None:
    mock_release.kube_client_mock.core_v1.list_namespaced_pod.return_value = V1PodList(
        items=[V1Pod(metadata=V1ObjectMeta(name="my_pod"))]
    )

    exec_mock = mock_release.kube_client_deploy_mock.with_mocked_exec(
        True,
        json.dumps(
            {
                "jobs": [{"status": "RUNNING"}],
            }
        ),
    )
    result = flink_status_via_rest(
        mock_release.build_release("my_ns", "cluster", "my_release")
    )

    mock_release.kube_client_mock.core_v1.list_namespaced_pod.assert_called_with(
        namespace="my_ns", label_selector="release=my_release,component=jobmanager"
    )
    assert exec_mock.call_args.args[1] == "my_pod"
    assert exec_mock.call_args.kwargs["namespace"] == "my_ns"
    assert exec_mock.call_args.kwargs["container"] == "flink-main-container"
    assert "python3" in exec_mock.call_args.kwargs["command"]
    assert result == "RUNNING"
