# Cirrus Reindex Orchestrator

The project orchestrates multi-wiki reindexes for CirrusSearch. It integrates
with SUP to handle backfilling operations. The process uses a replayable event
log for state management. If the script crashes or otherwise exits without
completing it can be run again with the same arguments to continue.

NOTE: the latest versions (0.5 and onwards) are not yet stable. Use a 0.4.x version
while https://phabricator.wikimedia.org/T382398 is sorted.

## Build and Deploy

### Lints and Tests

Uses python standard tox, configured via setup.cfg, for managing test environments
and commands.

### black

black is used to format python code before commiting. It is checked as part of the
CI pipeline by running bare `tox`.  To reformat the repository:

    tox -e black-fix

### isort

isort is used to sort imports before commiting. It is checked as part of the CI
pipeline be running bare `tox`. To reformat the repository:

    tox -e isort-fix

### Packaging

A python wheel package is build by the data platform engineering teams
`python_lib_repo` CI pipeline. Select `trigger_release` from the `Pipelines`
tab in gitlab for the top commit to the main branch to release the next
version and upload it to the project pypi index.

### Deployment

While more can be done, for now the module has no external requirements and
can be installed directly to a virtualenv without accessing the internet
for dependencies.

    $ mkdir ~/cirrus-reindex.$(date '+%Y%m%d')
    $ cd !$
    $ python3 -m venv --system-site-packages venv
    $ venv/bin/pip install "cirrus-reindexer >= 0.4.0, < 0.5.0" \
        --index-url https://gitlab.wikimedia.org/api/v4/groups/308/-/packages/pypi/simple

## Example invocations

Reindex all wikis in eqiad. This will create a directory in the current working
directory named `eqiad`, and within it will be both the state.json recording
the orchestration state, and per-invocation logs of reindex and backfill
operations.

Private wikis are excluded because SUP does not currently support private wikis.

    $ python3 -m cirrus_reindexer.reindex_all \
        --cirrus-cluster=eqiad \
        --exclude-dbnames @private

## Canceling a run

The orchestration includes support for cleanly shutting down after receiving
SIGINT (Ctrl-C). Once this signal is received no new reindexing operations will
be started. All in-progress tasks will continue, including potentially starting
multiple backfills.

If SIGINT is received multiple times within a five second window the orchestration
will reach out to the elasticsearch clusters and cancel all in-progress reindex
tasks. Even those not issued by this script (rare). The reindex failure will
be recognized by cirrus and follow the regular cleanup path.

All backfill operations will still complete. No support exists to cancel or
prevent a backfill from starting. Backfills will always be applied before
shutting down.

The run can be continued at any time after it has exited by re-running the
orchestration with the same parameters as before.
